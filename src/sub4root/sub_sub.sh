#########################################################################
# File Name: sub_sub.sh
# Author: Bernold Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: Sun 21 Jul 2019 03:16:35 PM CST
#########################################################################
#!/bin/bash


inputName=${1}
echo ${inputName}
root -l -b -q /afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/src/ST/LbdPi.cxx\(\"${inputName}\"\)
