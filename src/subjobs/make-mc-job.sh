#!/bin/bash
set +x

ecms=$1

#pthe data file
dpath1=/afs/ihep.ac.cn/users/h/hancj/hancj/EX20190723/ROOTFILE/rootFile

dataFiles=`ls $dpath1/**root `
fileNumber=`ls $dpath1/**root | wc -l`

##the data file number in one job
let nDataFile=5

##the job name with the format $myjob$iJob
let NUM=1
iJob=`expr 100000 + $NUM | cut -c 4-6`
myjob=${ecms}-

mkdir ${ecms}
cd ${ecms}
touch sub.sh
##change something below ONLY when you know what you are dong
let n=0
for recFile in $dataFiles
do 

  #echo $recFile
  let m=n+1

  if [[ ! -e $recFile ]]; then 
    echo "this data file $recFile doesn't exist! "
  else
 
    line="$recFile"

    if [[ $n%$nDataFile -eq 0 ]]; then
	  if [[ -e 'inputs.txt' ]]; then
		  echo "cd ${myjob}${iJob}" >> ../sub.sh
          echo "hep_sub sub_sub.sh" >> ../sub.sh
		  echo "cd .." >> ../sub.sh
          let NUM++
          iJob=`expr 100000 + $NUM | cut -c 4-6`
		  cd ../
	  fi
      mkdir  ${myjob}${iJob}
      cd  ${myjob}${iJob}
	  touch inputs.txt
	  echo "reco_flag 3" >> inputs.txt
	  echo "reco_veto 1" >> inputs.txt
	  touch sub_sub.sh
	  echo "root -l -b -q /afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/src/DT/${ecms}.cxx" >> sub_sub.sh
	  chmod 755 sub_sub.sh
    fi

    if [[ $m%$nDataFile -eq 0 || $m -eq $fileNumber ]]; then
      echo "reco_inputName ${line}" >> inputs.txt
    else
      echo "reco_inputName ${line}" >> inputs.txt
    fi


    let n++
  fi
done

##for the last jobOption file

echo "cd ${myjob}${iJob}" >> ../sub.sh
echo "hep_sub sub_sub.sh" >> ../sub.sh
echo "cd .." >> ../sub.sh

chmod +x ../sub.sh
cd ../..

echo "cd ${ecms}" >> sub.sh
echo "./sub.sh" >> sub.sh
echo "cd .." >> sub.sh

exit 0
