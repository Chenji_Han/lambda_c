/*************************************************************************
    > File Name: drawHist.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Tue 02 Jul 2019 08:42:43 PM CST
 ************************************************************************/
#include"bes3plotstyle.C"
#include"TTree.h"
#include"TFile.h"
#include"TH1.h"
#include"string.h"
#include<iostream>
#include"TCanvas.h"
#include"TStyle.h"
#include"functions.cxx"

using namespace std;

void draw_sub(TH1D* Hist,string title,string XTitle,string saveName){

//	SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C;

	Hist -> GetXaxis() -> SetTitle(XTitle.c_str());
	Hist -> GetYaxis() -> SetTitle("events");
	Hist -> SetTitle(title.c_str());
	Hist -> Draw("hist");

	C.SaveAs(saveName.c_str());

}

void drawHist(){

	vector<string> inputName;
	string title;
	double dE_min,dE_max,dE_bin;
	double Mbc_min,Mbc_max,Mbc_bin;
	double MXi_min,MXi_max,MXi_bin;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c,d;
        iss>>a>>b>>c>>d;

		if(a=="draw_dE"){
			dE_bin = string2double(b);
			dE_min = string2double(c);
			dE_max = string2double(d);
		}else if(a=="draw_Mbc"){
			Mbc_bin = string2double(b);
			Mbc_min = string2double(c);
			Mbc_max = string2double(d);
		}else if(a=="draw_title"){
			title = b;
		}else if(a=="draw_inputName"){
			inputName.push_back(b);
		}else if(a=="draw_MXi"){
			MXi_bin = string2double(b);
			MXi_min = string2double(c);
			MXi_max = string2double(d);
		}
	}

	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	double dE, Mbc, MXi;
	inputTree -> SetBranchAddress("dE",&dE);
	inputTree -> SetBranchAddress("Mbc",&Mbc);
	inputTree -> SetBranchAddress("MXi",&MXi);

	int passNum = 0;
	TH1D* HdE  = new TH1D("dE","",dE_bin,dE_min,dE_max);
	TH1D* HMbc = new TH1D("Mbc","",Mbc_bin,Mbc_min,Mbc_max);
	TH1D* HMXi = new TH1D("MXi","",MXi_bin,MXi_min,MXi_max);
	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
		if(ievt%1000==0) cout<<"---processing..."<<ievt<<endl;
		HdE  -> Fill(dE);
		HMbc -> Fill(Mbc);
		if(MXi_min<MXi&&MXi<MXi_max) passNum++;
		HMXi -> Fill(MXi);
	}

	if(inputName.size()==1){
		draw_sub(HdE,"#DeltaE distribution of reconstructed "+title,"E/GeV","dE.png");
		draw_sub(HMbc,"invariant mass distribution of reconstructed "+title,"mass/GeV","Mbc.png");
		draw_sub(HMXi,"mass distribution of reconstructed #Xi^{+} in "+title+" DT channel","mass/GeV","MXi.png");
	}else{
		draw_sub(HdE,"#DeltaE distribution of reconstructed#Lambda^{+}_{c}","E/GeV","dE.png");
		draw_sub(HMbc,"invariant mass distribution of reconstructed#Lambda^{+}_{c}","mass/GeV","Mbc.png");
		draw_sub(HMXi,"mass distribution of reconstructed #Xi^{-} in DT channels","mass/GeV","MXi.png");
	
	}


	cout<<"total reconstructed Xi "<<passNum<<endl;
	return;

}
