/*************************************************************************
    > File Name: LbdPi.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 02:35:54 AM CST
 ************************************************************************/

#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void DT(){

	vector<string> inputName;
	double dE_min, dE_max;
	double Mbc_min, Mbc_max;
	int VFLswitch, VFXswitch;
	int PID_proton, PID_pion;

	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="DT_dE"){
			dE_min = string2double(b);
			dE_max = string2double(c);
		}else if(a=="DT_Mbc"){
			Mbc_min = string2double(b);
			Mbc_max = string2double(c);
		}else if(a=="DT_inputName"){
			inputName.push_back(b);
		}else if(a=="DT_VFLswitch"){
			VFLswitch =  string2double(b);
		}else if(a=="DT_VFXswitch"){
			VFXswitch =  string2double(b);
		}else if(a=="DT_PID_proton"){
			PID_proton = (int) string2double(b);
		}

	}


	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<int>* STindex;
	vector<vector<double> >* Xi;
	double dE;
	double Mbc;

	inputTree -> SetBranchAddress("Xi",&Xi);
	inputTree -> SetBranchAddress("STindex",&STindex);
	inputTree -> SetBranchAddress("dE",&dE);
	inputTree -> SetBranchAddress("Mbc",&Mbc);

	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_DT" + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE_w=1.0, Mbc_w=0.0, MXi_w=0.0;

	outputTree -> Branch("dE",&dE_w);
	outputTree -> Branch("Mbc",&Mbc_w);
	outputTree -> Branch("MXi",&MXi_w);

	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
		if(ievt%100==0) cout<<"processing..."<<ievt<<endl;
		
		if(!(dE_min<dE&&dE<dE_max)) continue;
		if(!(Mbc_min<Mbc&&Mbc<Mbc_max)) continue;
		if(!(Xi->size()>0)) continue;

		double M_Xi_temp = 0;
		bool pass = false;

		for(int i=0;i<Xi->size();i++){
			vector<double> Xi_temp = Xi -> at(i);
			
			if(IsOverlap(Xi_temp,STindex)) continue;
			
			//if(!()) continue; // cut on the chi2 of lbd

			if(!(Xi_temp[8]<500)) continue;

			if(VFLswitch==-1){
				double Lbd_decay_length = Xi_temp[9];
				double Lbd_decay_error  = Xi_temp[10];
				if(!(Lbd_decay_length>2*Lbd_decay_error)) continue;
			}else{
				double Lbd_decay_length = Xi_temp[9];
				if(!(Lbd_decay_length>VFLswitch)) continue;
			}

			if(VFXswitch==-1){
				double Xi_decay_length  = Xi_temp[11];
				double Xi_decay_error   = Xi_temp[12];
				if(!(Xi_decay_length>2*Lbd_decay_error)) continue;
			}else{
				double Xi_decay_length = Xi_temp[11];
				if(!(Xi_decay_length>VFXswitch)) continue;
			}

			if(PID_proton==1){
				double PID_proton_valid = Xi_temp[20];
				double PID_proton_p     = Xi_temp[21];
				double PID_proton_k     = Xi_temp[22];
				double PID_proton_pi    = Xi_temp[23];
				if(!((int)PID_proton_valid==0)) continue;
				if(!(PID_proton_p>0)) continue;
				if(!(PID_proton_p>PID_proton_k)) continue;
				if(!(PID_proton_p>PID_proton_pi)) continue;
			}

			pass = true;
			if(TMath::Abs(Xi_temp[3]-1.32171)<TMath::Abs(M_Xi_temp-1.32171)){
				M_Xi_temp = Xi_temp[3];
			}
		}

		if(pass==true){
			MXi_w = M_Xi_temp;
			dE_w  = dE;
			Mbc_w = Mbc;
			outputTree -> Fill();
		}
	
	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();


}





