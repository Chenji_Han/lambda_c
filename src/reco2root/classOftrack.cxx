/*************************************************************************
    > File Name: tracks.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 17 Jun 2019 10:23:34 PM CST
 ************************************************************************/

#include "LambdacTagAlg/Lambdac.h"
TRACK::TRACK(TLorentzVector momentum,int num){
	Momentum = new TLorentzVector(momentum.Px(),momentum.Py(),momentum.Pz(),momentum.E());
	Index    = num;
	Mass     = momentum.M();
}
TLorentzVector * TRACK::GetMomentum(){
	return Momentum;
}
double TRACK::GetMass(){
	return Mass;
}
int TRACK::GetIndex(){
	return Index;
}

PI0::PI0(TLorentzVector gam1,TLorentzVector gam2,TLorentzVector pi0,int num1,int num2){
	MGam1 = new TLorentzVector(gam1.Px(),gam1.Py(),gam1.Pz(),gam1.E());
	MGam2 = new TLorentzVector(gam2.Px(),gam2.Py(),gam2.Pz(),gam2.E());
	MPi0  = new TLorentzVector(pi0.Px(),pi0.Py(),pi0.Pz(),pi0.E());
	Mass  = pi0.M();
	Index1= num1;
	Index2= num2;
}
TLorentzVector* PI0::GetMGam1(){
	return MGam1;
}
TLorentzVector* PI0::GetMGam2(){
	return MGam2;
}
TLorentzVector* PI0::GetMPi0(){
	return MPi0;
}
double PI0::GetMass(){
	return Mass;
}
int PI0::GetIndex1(){
	return Index1;
}
int PI0::GetIndex2(){
	return Index2;
}



KS0::KS0(TLorentzVector pip,TLorentzVector pim,TLorentzVector Ks0,int num1,int num2){
	MPip = new TLorentzVector(pip.Px(),pip.Py(),pip.Pz(),pip.E());
	MPim = new TLorentzVector(pim.Px(),pim.Py(),pim.Pz(),pim.E());
	MKs0 = new TLorentzVector(Ks0.Px(),Ks0.Py(),Ks0.Pz(),Ks0.E());
	Mass  = Ks0.M();
	PipIndex= num1;
	PimIndex= num2;
}
TLorentzVector* KS0::GetMPip(){
	return MPip;
}
TLorentzVector* KS0::GetMPim(){
	return MPim;
}
TLorentzVector* KS0::GetMKs0(){
	return MKs0;
}
double KS0::GetMass(){
	return Mass;
}
int* KS0::GetIndex(){
	int num[2];
	num[0] = PipIndex;
	num[1] = PimIndex;
	return num;
}


LBD::LBD(TLorentzVector p,TLorentzVector pi,TLorentzVector Lbd,int num1,int num2){
	MP = new TLorentzVector(p.Px(),p.Py(),p.Pz(),p.E());
	MPi = new TLorentzVector(pi.Px(),pi.Py(),pi.Pz(),pi.E());
	MLbd = new TLorentzVector(Lbd.Px(),Lbd.Py(),Lbd.Pz(),Lbd.E());
	Mass  = Lbd.M();
	PIndex= num1;
	PiIndex= num2;
}
TLorentzVector* LBD::GetMP(){
	return MP;
}
TLorentzVector* LBD::GetMPi(){
	return MPi;
}
TLorentzVector* LBD::GetMLbd(){
	return MLbd;
}
double LBD::GetMass(){
	return Mass;
}
int LBD::GetIndex1(){
	return PIndex;
}
int LBD::GetIndex2(){
	return PiIndex;
}

SIGMA0::SIGMA0(TLorentzVector lbdp,TLorentzVector lbdpi,TLorentzVector lbd,TLorentzVector gam,TLorentzVector sigma0,int num1,int num2,int num3){
	MLbdP = new TLorentzVector(lbdp.Px(),lbdp.Py(),lbdp.Pz(),lbdp.E());
	MLbdPi = new TLorentzVector(lbdpi.Px(),lbdpi.Py(),lbdpi.Pz(),lbdpi.E());
	MLbd = new TLorentzVector(lbd.Px(),lbd.Py(),lbd.Pz(),lbd.E());
	MGam = new TLorentzVector(gam.Px(),gam.Py(),gam.Pz(),gam.E());
	MSigma0 = new TLorentzVector(sigma0.Px(),sigma0.Py(),sigma0.Pz(),sigma0.E());
	Mass  = sigma0.M();
	LbdPIndex= num1;
	LbdPiIndex= num2;
	GamIndex = num3;
}
TLorentzVector* SIGMA0::GetMLbdP(){
	return MLbdP;
}
TLorentzVector* SIGMA0::GetMLbdPi(){
	return MLbdPi;
}
TLorentzVector* SIGMA0::GetMLbd(){
	return MLbd;
}
TLorentzVector* SIGMA0::GetMGam(){
	return MGam;
}
TLorentzVector* SIGMA0::GetMSigma0(){
	return MSigma0;
}
double SIGMA0::GetMass(){
	return Mass;
}
int* SIGMA0::GetIndex(){
	int num[3];
	num[0] = LbdPIndex;
	num[1] = LbdPiIndex;
	num[2] = GamIndex;
	return num;
}


SIGMA::SIGMA(TLorentzVector p,TLorentzVector gam1,TLorentzVector gam2,TLorentzVector pi0,TLorentzVector sigma,int num1,int num2,int num3){
	MP = new TLorentzVector(p.Px(),p.Py(),p.Pz(),p.E());
	MGam1 = new TLorentzVector(gam1.Px(),gam1.Py(),gam1.Pz(),gam1.E());
	MGam2 = new TLorentzVector(gam2.Px(),gam2.Py(),gam2.Pz(),gam2.E());
	MPi0 = new TLorentzVector(pi0.Px(),pi0.Py(),pi0.Pz(),pi0.E());
	MSigma = new TLorentzVector(sigma.Px(),sigma.Py(),sigma.Pz(),sigma.E());
	Mass  = sigma.M();
	PIndex = num1;
	Gam1Index = num2;
	Gam2Index= num3;
}
TLorentzVector* SIGMA::GetMP(){
	return MP;
}
TLorentzVector* SIGMA::GetMGam1(){
	return MGam1;
}
TLorentzVector* SIGMA::GetMGam2(){
	return MGam2;
}
TLorentzVector* SIGMA::GetMPi0(){
	return MPi0;
}
TLorentzVector* SIGMA::GetMSigma(){
	return MSigma;
}
double SIGMA::GetMass(){
	return Mass;
}
int* SIGMA::GetIndex(){
	int num[3];
	num[0] = PIndex;
	num[1] = Gam1Index;
	num[2] = Gam2Index;
	return num;
}







