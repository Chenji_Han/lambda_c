/************************************************************************
  > File Name: Lambdac.cxx
  > Author: Chen-Ji Han
  > Mail: hanchenji16@mails.ucas.ac.cn 
  > Created Time: Thu 16 Aug 2018 10:51:06 AM CST
 ************************************************************************/
#include "LambdacTagAlg/Lambdac.h"
#include "Xi_reco.cxx"
#include "classOftrack.cxx"
#include "Tracks_reco.cxx"

using Event::McParticle;
using Event::McParticleCol;

int ievt=0;


Lambdac::Lambdac(const std::string& name, ISvcLocator* pSvcLocator):Algorithm(name, pSvcLocator) {
	//	do nothing  
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode Lambdac::initialize(){
	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "in initialize()" << endmsg;

	//next codes are used to initialize the match service
	IMCTruthMatchSvc *i_matchSvc;
	StatusCode sc_MC = service("MCTruthMatchSvc",i_matchSvc);
	if(sc_MC.isFailure()){
		cout<<"MCTruthMatchSvc load fail"<<endl;
		return 0;
	}
	matchSvc=dynamic_cast<MCTruthMatchSvc*>(i_matchSvc);

	matchSwitch = "0";
	string outputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="dst_outputName"){
			outputName = b;
		}else if(a=="dst_matchSwitch"){
			matchSwitch = b;
		}
	}

	outputFile = new TFile(outputName.c_str(),"recreate");
	outputTree = new TTree("output","");
	
	outputTree->Branch("runNumber",&runNumber);
	outputTree->Branch("eventNumber",&eventNumber);
	
	outputTree->Branch("AllTracks",&AllTracks);
	outputTree->Branch("ProtonP",&ProtonP);
	outputTree->Branch("ProtonM",&ProtonM);
	outputTree->Branch("PionP",&PionP);
	outputTree->Branch("PionM",&PionM);
	outputTree->Branch("KaonP",&KaonP);
	outputTree->Branch("KaonM",&KaonM);
	outputTree->Branch("Gamma",&Gamma);

	outputTree->Branch("Pi0",&Pi0);
	outputTree->Branch("Ks0",&Ks0);
	outputTree->Branch("Lbd",&Lbd);
	outputTree->Branch("ALbd",&ALbd);
	outputTree->Branch("Sigma0",&Sigma0);
	outputTree->Branch("ASigma0",&ASigma0);
	outputTree->Branch("SigmaP",&SigmaP);
	outputTree->Branch("SigmaM",&SigmaM);
	
	outputTree->Branch("Xi",&Xi);
	outputTree->Branch("XiBar",&XiBar);
	
	log << MSG::INFO << "successfully return from initialize()" <<endmsg;
	return StatusCode::SUCCESS;

}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
StatusCode Lambdac::execute() {

	ievt++;	if(ievt%10000==0) cout<<"processing..."<<ievt<<endl;

	SmartDataPtr<Event::EventHeader> eventHeader(eventSvc(),"/Event/EventHeader");
	SmartDataPtr<EvtRecEvent> evtRecEvent(eventSvc(), EventModel::EvtRec::EvtRecEvent);
	SmartDataPtr<EvtRecTrackCol> evtRecTrkCol(eventSvc(),  EventModel::EvtRec::EvtRecTrackCol);
	
	runNumber=eventHeader->runNumber();
	eventNumber=eventHeader->eventNumber();

	// charged track
	vector<vector<double> > tracks_tem;
	Vint Track_10;Track_10.clear();
	IsGoodTrack(evtRecEvent,evtRecTrkCol,10.0,1.0,Track_10,tracks_tem);
	Vint Track_20;Track_20.clear();
	IsGoodTrack(evtRecEvent,evtRecTrkCol,20.0,-1.0,Track_20,tracks_tem);
	Vint Track_Xi; Track_Xi.clear();
	AllTracks.clear();
	IsGoodTrack(evtRecEvent,evtRecTrkCol,-1.0,-1.0,Track_Xi,AllTracks);

	// gammma
	Vint Gam_Track; Gam_Track.clear();
	Vp4 Gam_EP; Gam_EP.clear();
	Gamma.clear();
	IsGammatrack(evtRecEvent, evtRecTrkCol,Gam_Track, Gam_EP, Gamma);

	Vint ProtonP_Track,ProtonM_Track; ProtonP_Track.clear();ProtonM_Track.clear();
	Vp4 ProtonP_EP,ProtonM_EP;ProtonP_EP.clear();ProtonM_EP.clear();
	ProtonP.clear(); ProtonM.clear();
	IsProtonM(1, evtRecTrkCol,Track_10,ProtonM_EP,ProtonM_Track,ProtonM);
	IsProtonP(1, evtRecTrkCol,Track_10,ProtonP_EP,ProtonP_Track,ProtonP);

	Vint KaonP_Track,KaonM_Track; KaonP_Track.clear();KaonM_Track.clear();
	Vp4 KaonP_EP,KaonM_EP;KaonP_EP.clear();KaonM_EP.clear();
	KaonM.clear(); KaonP.clear();
	IsKaonM(1, evtRecTrkCol,Track_10,KaonM_EP,KaonM_Track,KaonM);
	IsKaonP(1, evtRecTrkCol,Track_10,KaonP_EP,KaonP_Track,KaonP);

	Vint PionP_Track,PionM_Track; PionP_Track.clear();PionM_Track.clear();
	Vp4 PionP_EP,PionM_EP;PionP_EP.clear();PionM_EP.clear();
	PionP.clear(); PionM.clear();	
	IsPionM(1, evtRecTrkCol,Track_10,PionM_EP,PionM_Track,PionM);
	IsPionP(1, evtRecTrkCol,Track_10,PionP_EP,PionP_Track,PionP);

	Vint PionM_loose,PionP_loose; PionM_loose.clear(); PionP_loose.clear();
	Vp4 PionM_ep,PionP_ep; PionM_ep.clear();PionP_ep.clear();
	vector<vector<double> > vector_tem;
	IsPionM(-1,evtRecTrkCol,Track_20,PionM_ep,PionM_loose,vector_tem);
	IsPionP(-1,evtRecTrkCol,Track_20,PionP_ep,PionP_loose,vector_tem);

	Vint PionP_Xi; PionP_Xi.clear(); 
	Vp4  PionP_ep_Xi; PionP_ep_Xi.clear();
	Vint PionM_Xi; PionM_Xi.clear(); 
	Vp4  PionM_ep_Xi; PionM_ep_Xi.clear();
	IsPionM(-1,evtRecTrkCol,Track_Xi,PionM_ep_Xi,PionM_Xi,vector_tem);
	IsPionP(-1,evtRecTrkCol,Track_Xi,PionP_ep_Xi,PionP_Xi,vector_tem);

	Vint ProtonP_Xi; ProtonP_Xi.clear(); 
	Vp4  ProtonP_ep_Xi; ProtonP_ep_Xi.clear();
	Vint ProtonM_Xi; ProtonM_Xi.clear(); 
	Vp4  ProtonM_ep_Xi; ProtonM_ep_Xi.clear();
	IsProtonM(-1,evtRecTrkCol,Track_Xi,ProtonM_ep_Xi,ProtonM_Xi,vector_tem);
	IsProtonP(-1,evtRecTrkCol,Track_Xi,ProtonP_ep_Xi,ProtonP_Xi,vector_tem);

	Vint ProtonP_loose; ProtonP_loose.clear();
	Vint ProtonM_loose; ProtonM_loose.clear();
	Vp4 ProtonP_loose_ep; ProtonP_loose_ep.clear();
	Vp4 ProtonM_loose_ep; ProtonM_loose_ep.clear();
	IsProtonM(1,evtRecTrkCol,Track_20,ProtonM_loose_ep,ProtonM_loose,vector_tem);
	IsProtonP(1,evtRecTrkCol,Track_20,ProtonP_loose_ep,ProtonP_loose,vector_tem);
	vector_tem.clear();

	// next are for the indirect tracks
	
	Xi.clear(); XiBar.clear();
	IsXi(evtRecTrkCol,ProtonP_Xi,PionM_Xi,Xi);
	IsXi(evtRecTrkCol,ProtonM_Xi,PionP_Xi,XiBar);

	vector<LBD> Lbd_class,ALbd_class;
	Lbd.clear(); ALbd.clear();
	IsLbd(evtRecEvent,evtRecTrkCol,ProtonP_loose,ProtonM_loose,PionM_loose,PionP_loose,Lbd_class,ALbd_class,Lbd,ALbd);

	vector<PI0> Pi0_class;
	Pi0.clear();
	IsPi0(evtRecTrkCol,Gam_EP,Gam_Track,Pi0_class,Pi0);

	Ks0.clear();
	IsKs0(evtRecTrkCol,PionP_loose,PionM_loose,Ks0);

	SigmaP.clear(); SigmaM.clear();
	IsSigmam(evtRecTrkCol,ProtonM_loose_ep,ProtonM_loose,Pi0_class,SigmaM);
	IsSigmap(evtRecTrkCol,ProtonP_loose_ep,ProtonP_loose,Pi0_class,SigmaP);

	Sigma0.clear(); ASigma0.clear();
	IsSigma0(Lbd_class,ALbd_class,Gam_EP,Gam_Track,Sigma0,ASigma0);


	outputTree -> Fill();



	return StatusCode::SUCCESS;
}


StatusCode Lambdac::finalize() {


	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	MsgStream log(msgSvc(), name());
	log << MSG::INFO << "in finalize()" << endmsg;
	return StatusCode::SUCCESS;
}

