/*************************************************************************
    > File Name: Tracks_class.h
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 16 Jun 2019 01:44:12 AM CST
 ************************************************************************/
#ifndef Tracks_class_H
#define Tracls_class_H

/**** the class used to store the info of P, Pi, K, Gam ****/
class TRACK {

	public:
		TRACK(TLorentzVector,int);
		TLorentzVector * GetMomentum();
		double GetMass();
		int GetIndex();

	private:
		TLorentzVector * Momentum;
		double Mass;
		int Index;
};

/****    the class used to store the info of Pi0 ****/
class PI0 {

	public:
		PI0(TLorentzVector, TLorentzVector,TLorentzVector,int,int);
		TLorentzVector * GetMGam1();
		TLorentzVector * GetMGam2();
		TLorentzVector * GetMPi0();
		double GetMass();
		int GetIndex1();
		int GetIndex2();

	private:
		TLorentzVector * MGam1;
		TLorentzVector * MGam2;
		TLorentzVector * MPi0;
		double Mass;
		int	Index1;
		int Index2;
	
};



/**** next is the class used to store the info of the Ks0 ****/
class KS0 {

	public:
		KS0(TLorentzVector, TLorentzVector,TLorentzVector,int,int);
		TLorentzVector * GetMPip();
		TLorentzVector * GetMPim();
		TLorentzVector * GetMKs0();
		double GetMass();
		int * GetIndex();

	private:
		TLorentzVector * MPip;
		TLorentzVector * MPim;
		TLorentzVector * MKs0;
		double Mass;
		int	PipIndex;
		int PimIndex;
	
};
/**** next is the class used to store the info of the Lambda ****/
class LBD {

	public:
		LBD(TLorentzVector,TLorentzVector,TLorentzVector,int,int);
		TLorentzVector * GetMP();
		TLorentzVector * GetMPi();
		TLorentzVector * GetMLbd();
		double GetMass();
		int GetIndex1();
		int GetIndex2();

	private:
		TLorentzVector * MP;
		TLorentzVector * MPi;
		TLorentzVector * MLbd;
		double Mass;
		int	PIndex;
		int PiIndex;
	
};
/**** next is the class used to store the info of the Sigma0 ****/
class SIGMA0 {

	public:
		SIGMA0(TLorentzVector,TLorentzVector,TLorentzVector,TLorentzVector,TLorentzVector,int,int,int);
		TLorentzVector * GetMLbdP();
		TLorentzVector * GetMLbdPi();
		TLorentzVector * GetMLbd();
		TLorentzVector * GetMGam();
		TLorentzVector * GetMSigma0();
		double GetMass();
		int * GetIndex();

	private:
		TLorentzVector * MLbdP;
		TLorentzVector * MLbdPi;
		TLorentzVector * MLbd;
		TLorentzVector * MGam;
		TLorentzVector * MSigma0;
		double Mass;
		int	LbdPIndex;
		int LbdPiIndex;
		int GamIndex;
	
};

/**** next is the class used to store the info of the Sigma ****/
class SIGMA {

	public:
		SIGMA(TLorentzVector,TLorentzVector,TLorentzVector,TLorentzVector,TLorentzVector,int,int,int);
		TLorentzVector * GetMP();
		TLorentzVector * GetMGam1();
		TLorentzVector * GetMGam2();
		TLorentzVector * GetMPi0();
		TLorentzVector * GetMSigma();
		double GetMass();
		int * GetIndex();

	private:
		TLorentzVector * MP;
		TLorentzVector * MGam1;
		TLorentzVector * MGam2;
		TLorentzVector * MPi0;
		TLorentzVector * MSigma;
		double Mass;
		int	PIndex;
		int Gam1Index;
		int Gam2Index;
	
};


#endif



