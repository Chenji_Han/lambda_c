/*************************************************************************
    > File Name: Xi_reco.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Wed 26 Jun 2019 07:33:55 PM CST
 ************************************************************************/

#include "LambdacTagAlg/Lambdac.h"


void Lambdac::IsXi(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint ProtonP_Xi,Vint PionM_Xi,vector<vector<double> >& Xi){

	Hep3Vector ip0(0,0,0);
	HepSymMatrix ip0Ex(3,0);
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);         //???????????????????????????
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex();
		double* vv  = vtxsvc->SigmaPrimaryVertex();
		ip0.setX(dbv[0]);
		ip0.setY(dbv[1]);
		ip0.setZ(dbv[2]);
		ip0Ex[0][0] = vv[0] * vv[0];
		ip0Ex[1][1] = vv[1] * vv[1];
		ip0Ex[2][2] = vv[2] * vv[2];
	}else{
		 cout << "GetIP Failure" << endl;
		return;
	}


	// * * * * * * * * * * * VertexFit and SecondVertexFit of Lambda and Xi * * * * * * * * * * * * *
	VertexFit       *vtxfit  = VertexFit::instance();
	SecondVertexFit *svtxfit = SecondVertexFit::instance();

	// To Store 4-momentum of All Particles
	HepLorentzVector P4_Xi;

	// Initialized Before VertexFit, Used For VertexFit
	WTrackParameter wtrkproton;
	WTrackParameter wtrkpi1;
	WTrackParameter wtrkpi2;

	// Assigned After VertexFit, WTrackParameter Renewed by VertexFit
	WTrackParameter wXi;
	WTrackParameter wlambda;

	// Interaction Point v0, v1, v2
	VertexParameter v0par;
	VertexParameter v1par;
	VertexParameter v2par;

	double chi2_VFL   = 9999.;
	double chi2_VFX   = 9999.;
	double chi2_SVFL  = 9999.;
	double chi2_SVFX  = 9999.;
	double len_lambda = -999.;
	double len_Xi     = -999.;

	double Delta  = 2.0;
	int    ok_fit = 0;


	HepPoint3D    vx(0., 0., 0.);
	HepSymMatrix  Evx(3, 0);
	double bx = 1E+6;
	double by = 1E+6;
	double bz = 1E+6;
	Evx[0][0] = bx * bx;
	Evx[1][1] = by * by;
	Evx[2][2] = bz * bz;
	VertexParameter vxpar_lambda;
	vxpar_lambda.setVx(vx);
	vxpar_lambda.setEvx(Evx);
	VertexParameter vxpar_Xi;
	vxpar_Xi.setVx(vx);
	vxpar_Xi.setEvx(Evx);

	for(int i = 0; i < ProtonP_Xi.size(); i++){
		RecMdcKalTrack *protonTrk = (*(evtRecTrkCol->begin()+ProtonP_Xi[i]))->mdcKalTrack();
		protonTrk->setPidType(RecMdcKalTrack::proton);
		wtrkproton = WTrackParameter(mp, protonTrk->getZHelixP(), protonTrk->getZErrorP());
		for(int j = 0; j < PionM_Xi.size(); j++){
			RecMdcKalTrack *pi1Trk = (*(evtRecTrkCol->begin()+PionM_Xi[j]))->mdcKalTrack();
			pi1Trk->setPidType(RecMdcKalTrack::pion);
			wtrkpi1 = WTrackParameter(mpi, pi1Trk->getZHelix(), pi1Trk->getZError());
			vtxfit->init();
			vtxfit->AddTrack(0, wtrkproton);
			vtxfit->AddTrack(1, wtrkpi1);
			vtxfit->AddVertex(0, vxpar_lambda, 0, 1);
			if(!vtxfit->Fit(0)) continue;
			vtxfit->Swim(0);
			vtxfit->BuildVirtualParticle(0);
			wlambda = vtxfit->wVirtualTrack(0);
			v2par   = vtxfit->vpar(0);
			double chi2_VFL_temp = vtxfit->chisq(0);
			for(int k = 0; k < PionM_Xi.size(); k++){
				if(j == k) continue;
				RecMdcKalTrack *pi2Trk = (*(evtRecTrkCol->begin()+PionM_Xi[k]))->mdcKalTrack();
				pi2Trk->setPidType(RecMdcKalTrack::pion);
				wtrkpi2 = WTrackParameter(mpi, pi2Trk->getZHelix(), pi2Trk->getZError());
				vtxfit->init();
				vtxfit->AddTrack(0, wlambda);
				vtxfit->AddTrack(1, wtrkpi2);
				vtxfit->AddVertex(0, vxpar_Xi, 0, 1);
				if(!vtxfit->Fit(0)) continue;
				vtxfit->Swim(0);
				vtxfit->BuildVirtualParticle(0);
				wXi   = vtxfit->wVirtualTrack(0);
				v1par = vtxfit->vpar(0);
				double chi2_VFX_temp = vtxfit->chisq(0);

				svtxfit->init();
				svtxfit->setPrimaryVertex(v1par);
				svtxfit->AddTrack(0, wlambda);
				svtxfit->setVpar(v2par);
				if(!svtxfit->Fit()) continue;
	
				HepLorentzVector P4_lambda_temp = svtxfit->p4par();
				double chi2_SVFL_temp  = svtxfit->chisq();
				double len_lambda_temp = svtxfit->decayLength();
		
				v0par.setVx(ip0);
				v0par.setEvx(ip0Ex);
				svtxfit->init();
				svtxfit->setPrimaryVertex(v0par);
				svtxfit->AddTrack(0, wXi);
				svtxfit->setVpar(v1par);
				if(!svtxfit->Fit()) continue;
				
				HepLorentzVector P4_Xi_temp = svtxfit->p4par();
				double chi2_SVFX_temp = svtxfit->chisq();
				double len_Xi_temp    = svtxfit->decayLength();
		
				if(!(chi2_SVFL_temp<500&&len_lambda_temp>0)) continue;
				if(!(len_Xi_temp>0)) continue;

				vector<double> vec_temp;
				vec_temp.push_back(P4_Xi_temp.px());
				vec_temp.push_back(P4_Xi_temp.py());
				vec_temp.push_back(P4_Xi_temp.pz());
				vec_temp.push_back(P4_Xi_temp.e());
				vec_temp.push_back(P4_Xi_temp.m());
				vec_temp.push_back(P4_lambda_temp.m());
				vec_temp.push_back(double(ProtonP_Xi[i]));
				vec_temp.push_back(double(PionM_Xi[j]));
				vec_temp.push_back(double(PionM_Xi[k]));
		
				Xi.push_back(vec_temp);

			}
		}
	}

}


