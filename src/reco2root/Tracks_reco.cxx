/*************************************************************************
    > File Name: Tracks_reco.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 04 Jul 2019 12:23:44 AM CST
 ************************************************************************/



#include "LambdacTagAlg/Lambdac.h"

int pion_ID    = 211;
int proton_ID  = 2212;
int kaon_ID    = 321;
int gamma_ID   = 22;


void Lambdac::IsSigma0(vector<LBD> Lbd,vector<LBD> ALbd,Vp4 Gam_EP,Vint Gam_Track,vector<vector<double> >& Sigma0,vector<vector<double> >& ASigma0){


	for(int i=0;i<Lbd.size();i++){
		for(int j=0;j<Gam_Track.size();j++){

			TLorentzVector mlbd_tem = *(Lbd[i].GetMLbd());
			TLorentzVector mgam_tem(Gam_EP[j].px(),Gam_EP[j].py(),Gam_EP[j].pz(),Gam_EP[j].e());
			TLorentzVector sigma0_tem = mlbd_tem + mgam_tem;
			double mass_tem = sigma0_tem.M();
			if(!(1.179<mass_tem&&mass_tem<1.203)) continue;

			int index[2];
			index[0] = (Lbd[i]).GetIndex1();
			index[1] = (Lbd[i]).GetIndex2();
			vector<double> tem;
			tem.push_back(sigma0_tem.Px());
			tem.push_back(sigma0_tem.Py());
			tem.push_back(sigma0_tem.Pz());
			tem.push_back(sigma0_tem.E());
			tem.push_back(double(index[0]));
			tem.push_back(double(index[1]));
			tem.push_back(double(Gam_Track[j]));
			Sigma0.push_back(tem);

		}
	}


	for(int i=0;i<ALbd.size();i++){
		for(int j=0;j<Gam_Track.size();j++){

			TLorentzVector mlbd_tem = *(ALbd[i].GetMLbd());
			TLorentzVector mgam_tem(Gam_EP[j].px(),Gam_EP[j].py(),Gam_EP[j].pz(),Gam_EP[j].e());
			TLorentzVector sigma0_tem = mlbd_tem + mgam_tem;
			double mass_tem = sigma0_tem.M();
			if(!(1.179<mass_tem&&mass_tem<1.203)) continue;

			int index[2];
			index[0] = (ALbd[i]).GetIndex1();
			index[1] = (ALbd[i]).GetIndex2();

			vector<double> tem;
			tem.push_back(sigma0_tem.Px());
			tem.push_back(sigma0_tem.Py());
			tem.push_back(sigma0_tem.Pz());
			tem.push_back(sigma0_tem.E());
			tem.push_back(double(index[0]));
			tem.push_back(double(index[1]));
			tem.push_back(double(Gam_Track[j]));
			ASigma0.push_back(tem);

		}
	}
}

void Lambdac::  IsSigmam(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vp4 ProtonM_EP,Vint ProtonM_track,vector<PI0> Pi0,vector<vector<double> >& Sigma){

	for(int i=0;i<Pi0.size();i++){
		for(int j=0;j<ProtonM_track.size();j++){

			TLorentzVector mpi0_tem = *(Pi0[i].GetMPi0());
			TLorentzVector mpm(ProtonM_EP[j].px(),ProtonM_EP[j].py(),ProtonM_EP[j].pz(),ProtonM_EP[j].e());
			TLorentzVector msigmam_tem = mpi0_tem + mpm;
			double mass = msigmam_tem.M();
			if(!(1.176<mass&&mass<1.200)) continue;
	
			int index[2];
			index[0] = (Pi0[i]).GetIndex1();
			index[1] = (Pi0[i]).GetIndex2();

			vector<double> tem;
			tem.push_back(msigmam_tem.Px());
			tem.push_back(msigmam_tem.Py());
			tem.push_back(msigmam_tem.Pz());
			tem.push_back(msigmam_tem.E());
			tem.push_back(double(ProtonM_track[j]));
			tem.push_back(double(index[0]));
			tem.push_back(double(index[1]));
			Sigma.push_back(tem);

		}
	}

}


void Lambdac:: IsSigmap(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vp4 ProtonP_EP,Vint ProtonP_track,vector<PI0> Pi0,vector<vector<double> >& Sigma){

	for(int i=0;i<Pi0.size();i++){
		for(int j=0;j<ProtonP_track.size();j++){

			TLorentzVector mpi0_tem = *(Pi0[i].GetMPi0());
			TLorentzVector mpp(ProtonP_EP[j].px(),ProtonP_EP[j].py(),ProtonP_EP[j].pz(),ProtonP_EP[j].e());
			TLorentzVector msigmap_tem = mpi0_tem + mpp;
			double mass = msigmap_tem.M();
			if(!(1.176<mass&&mass<1.200)) continue;

			int index[2];
			index[0] = (Pi0[i]).GetIndex1();
			index[1] = (Pi0[i]).GetIndex2();
			
			vector<double> tem;
			tem.push_back(msigmap_tem.Px());
			tem.push_back(msigmap_tem.Py());
			tem.push_back(msigmap_tem.Pz());
			tem.push_back(msigmap_tem.E());
			tem.push_back(double(ProtonP_track[j]));
			tem.push_back(double(index[0]));
			tem.push_back(double(index[1]));
			Sigma.push_back(tem);

		}
	}

}

void Lambdac::IsKs0(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint PionP_Track,Vint PionM_Track,vector<vector<double> >& Ks0){

	bool isGoodks(RecMdcKalTrack* pipTrk, RecMdcKalTrack* pimTrk,HepLorentzVector& p4_ks_1s);

	for(int i = 0; i < PionP_Track.size(); i++) 
	{
		for(int j = 0; j < PionM_Track.size(); j++) 
		{
			EvtRecTrackIterator itTrki = evtRecTrkCol->begin() + PionP_Track[i];
			RecMdcKalTrack *pipKalTrk = (*itTrki)->mdcKalTrack();

			EvtRecTrackIterator itTrkj = evtRecTrkCol->begin() + PionM_Track[j];
			RecMdcKalTrack *pimKalTrk = (*itTrkj)->mdcKalTrack();
			
			HepLorentzVector p4_ks_1s(0,0,0,0);

			if(isGoodks(pipKalTrk, pimKalTrk,p4_ks_1s)){
		
				vector<double> tem;
				tem.push_back(p4_ks_1s.px());
				tem.push_back(p4_ks_1s.py());
				tem.push_back(p4_ks_1s.pz());
				tem.push_back(p4_ks_1s.e());
				tem.push_back(double(PionP_Track[i]));
				tem.push_back(double(PionM_Track[j]));
				Ks0.push_back(tem);
			}	

		}
	}


}

bool isGoodks(RecMdcKalTrack* pipTrk, RecMdcKalTrack* pimTrk,HepLorentzVector& p4_ks_1s){



	double ks_1chis=-100;
	double ks_mass =-100;

	HepPoint3D vx(0., 0., 0.);
	HepSymMatrix Evx(3, 0);
	double bx = 1E+6;
	double by = 1E+6;
	double bz = 1E+6;
	Evx[0][0] = bx*bx;
	Evx[1][1] = by*by;
	Evx[2][2] = bz*bz;
	VertexParameter vxpar;
	vxpar.setVx(vx);
	vxpar.setEvx(Evx);

	VertexFit *vtxfit_s = VertexFit::instance();
	SecondVertexFit *vtxfit2 = SecondVertexFit::instance();
	RecMdcKalTrack::setPidType(RecMdcKalTrack::pion);
	WTrackParameter  wvpipksTrk, wvpimksTrk;
	wvpipksTrk = WTrackParameter(0.13957, pipTrk->getZHelix(),pipTrk->getZError());
	wvpimksTrk = WTrackParameter(0.13957, pimTrk->getZHelix(),pimTrk->getZError());

	//******primary vertex fit
	vtxfit_s->init();
	vtxfit_s->AddTrack(0, wvpipksTrk);
	vtxfit_s->AddTrack(1, wvpimksTrk);
	vtxfit_s->AddVertex(0, vxpar, 0, 1);
	bool okvs=vtxfit_s->Fit(0);

	if(!okvs) return false;

	vtxfit_s->Swim(0);
	vtxfit_s->BuildVirtualParticle(0);
	ks_1chis = vtxfit_s->chisq(0);

	if(!(ks_1chis<100)) return false;

	WTrackParameter  wvks = vtxfit_s->wVirtualTrack(0);
	p4_ks_1s=wvks.p();
	ks_mass = p4_ks_1s.m();

	if(!(ks_mass > 0.487 && ks_mass< 0.511)) return false;


	//******second vertex fit
	HepPoint3D newvx(0., 0., 0.);
	HepSymMatrix newEvx(3, 0);
	VertexParameter primaryVpar;
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(!(vtxsvc->isVertexValid())) {
		cout<<"Attention --!(vtxsvc->isVertexValid())"<<endl;
		return false;
	}
	double* db_vx = vtxsvc->PrimaryVertex();
	double* db_vx_err = vtxsvc->SigmaPrimaryVertex();
	newvx.setX(db_vx[0]);
	newvx.setY(db_vx[1]);
	newvx.setZ(db_vx[2]);
	newEvx[0][0] = db_vx_err[0]*db_vx_err[0];
	newEvx[1][1] = db_vx_err[1]*db_vx_err[1];
	newEvx[2][2] = db_vx_err[2]*db_vx_err[2];
	primaryVpar.setVx(newvx);
	primaryVpar.setEvx(newEvx);
	vtxfit2->init();
	vtxfit2->setPrimaryVertex(primaryVpar);
	vtxfit2->AddTrack(0, wvks);
	vtxfit2->setVpar(vtxfit_s->vpar(0));
	bool okv2=vtxfit2->Fit();
	if(!okv2) return false;

	double ks_dl  = vtxfit2->decayLength();
	double ks_dle = vtxfit2->decayLengthError();
	if(!(ks_dl>2*ks_dle)) return false;

	return true;
}

void Lambdac::IsKaonP(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Kaon_EP,Vint & Kaon_Track,vector<vector<double> >& Kaon){

	ParticleID * pid=ParticleID::instance();
	double m_prob_pid_pi,m_prob_pid_k;

	for(int i = 0; i < Track_10.size(); i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,kaon_ID,4122)||matchSvc->match(*itTrk,kaon_ID,-4122))){
				continue;
			}
		}


		if(open==1){
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_k>0&&m_prob_pid_k>m_prob_pid_pi))
				continue;
		}

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();

		mdcKalTrk->setPidType(RecMdcKalTrack::kaon);

		if(!(mdcKalTrk->charge()==1)) continue;
	
		double px = mdcKalTrk->px();
		double py = mdcKalTrk->py();
		double pz = mdcKalTrk->pz();
		double E  = sqrt(px*px + py*py + pz*pz + mk*mk);

		HepLorentzVector ptrk;
		ptrk.setPx(px);
		ptrk.setPy(py);
		ptrk.setPz(pz);
		ptrk.setE(E);
		Kaon_EP.push_back(ptrk);
		Kaon_Track.push_back(Track_10[i]);

		vector<double> tem;
		tem.push_back(px);
		tem.push_back(py);
		tem.push_back(pz);
		tem.push_back(E);
		tem.push_back(double(Track_10[i]));
		Kaon.push_back(tem);


	}


}

void Lambdac::IsKaonM(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Kaon_EP,Vint & Kaon_Track,vector<vector<double> >& Kaon){

	ParticleID * pid=ParticleID::instance();
	double m_prob_pid_pi,m_prob_pid_k;

	for(int i = 0; i < Track_10.size(); i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,kaon_ID,4122)||matchSvc->match(*itTrk,kaon_ID,-4122))){
				continue;
			}
		}


		if(open==1){
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_k>0&&m_prob_pid_k>m_prob_pid_pi))
				continue;
		}

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();
		mdcKalTrk->setPidType(RecMdcKalTrack::kaon);

		if(!(mdcKalTrk->charge()==-1)) continue;

		double px = mdcKalTrk->px();
		double py = mdcKalTrk->py();
		double pz = mdcKalTrk->pz();
		double E  = sqrt(px*px + py*py + pz*pz + mk*mk);

		HepLorentzVector ptrk;
		ptrk.setPx(px);
		ptrk.setPy(py);
		ptrk.setPz(pz);
		ptrk.setE(E);
		Kaon_EP.push_back(ptrk);
		Kaon_Track.push_back(Track_10[i]);

		ptrk.boost(-0.011,0,0);

		vector<double> tem;
		tem.push_back(px);
		tem.push_back(py);
		tem.push_back(pz);
		tem.push_back(E);
		tem.push_back(double(Track_10[i]));
		Kaon.push_back(tem);

	}


}

void Lambdac::IsProtonM(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Proton_EP,Vint & Proton_Track,vector<vector<double> >& ProtonM){

	int nGood_Proton = Track_10.size();
	int p=0;
	ParticleID * pid=ParticleID::instance();

	double m_prob_pid_pi,m_prob_pid_p,m_prob_pid_k;

	for(int i = 0; i < nGood_Proton; i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;
	
		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,proton_ID,4122)||matchSvc->match(*itTrk,proton_ID,-4122))){
				continue;
			}
		}

		if(open==1){
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon()|pid->onlyProton());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_p  = pid->probProton();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_p>0))
				continue;
			if(!(m_prob_pid_p>m_prob_pid_pi&&m_prob_pid_p>m_prob_pid_k))
				continue;
		}
		

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();

		mdcKalTrk->setPidType(RecMdcKalTrack::proton);
		int charge=mdcKalTrk->charge();
		if(!(charge==-1))
			continue;

		double px = mdcKalTrk->px();
		double py = mdcKalTrk->py();
		double pz = mdcKalTrk->pz();
		double E  = sqrt(px*px + py*py + pz*pz + mp*mp);

		HepLorentzVector ptrk;
		ptrk.setPx(px);
		ptrk.setPy(py);
		ptrk.setPz(pz);
		ptrk.setE(E);
		Proton_EP.push_back(ptrk);
		Proton_Track.push_back(Track_10[i]);

		vector<double> tem;
		tem.push_back(px);
		tem.push_back(py);
		tem.push_back(pz);
		tem.push_back(E);
		tem.push_back(double(Track_10[i]));
		ProtonM.push_back(tem);

	}


}

void Lambdac::IsProtonP(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Proton_EP,Vint & Proton_Track,vector<vector<double> >& ProtonP){

	int nGood_Proton = Track_10.size();
	ParticleID * pid=ParticleID::instance();

	int index=0;
	double m_prob_pid_pi,m_prob_pid_p,m_prob_pid_k;

	for(int i = 0; i < nGood_Proton; i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,proton_ID,4122)||matchSvc->match(*itTrk,proton_ID,-4122))){
				continue;
			}
		}

		if(open==1){
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon()|pid->onlyProton());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_p  = pid->probProton();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_p>0))
				continue;
			if(!(m_prob_pid_p>m_prob_pid_pi&&m_prob_pid_p>m_prob_pid_k))
				continue;
		}

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();

		mdcKalTrk->setPidType(RecMdcKalTrack::proton);
		int charge=mdcKalTrk->charge();
		if(!(charge==1))
			continue;

		double px = mdcKalTrk->px();
		double py = mdcKalTrk->py();
		double pz = mdcKalTrk->pz();
		double E  = sqrt(px*px + py*py + pz*pz + mp*mp);

		HepLorentzVector ptrk;
		ptrk.setPx(px);
		ptrk.setPy(py);
		ptrk.setPz(pz);
		ptrk.setE(E);
		Proton_EP.push_back(ptrk);
		Proton_Track.push_back(Track_10[i]);

		vector<double> tem;
		tem.push_back(px);
		tem.push_back(py);
		tem.push_back(pz);
		tem.push_back(E);
		tem.push_back(double(Track_10[i]));
		ProtonP.push_back(tem);

	}


}

void Lambdac::IsGammatrack(SmartDataPtr<EvtRecEvent> evtRecEvent, SmartDataPtr<EvtRecTrackCol> evtRecTrkCol, Vint & Gam_Track, Vp4 & Gam_EP, vector<vector<double> >& Gamma){

	bool isGoodShower(int i,SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol);

	Hep3Vector xorigin(0,0,0);
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex(); 
		double*  vv = vtxsvc->SigmaPrimaryVertex();  
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		return;
	}

	for(int i = evtRecEvent->totalCharged(); i< evtRecEvent->totalTracks(); i++){
	
		if(matchSwitch=="1"&&runNumber<0){
			EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + i ; 
			if(!(matchSvc->match(*itTrk,gamma_ID,4122)||matchSvc->match(*itTrk,gamma_ID,-4122))){
				continue;
			}
		}

		if(!isGoodShower(i,evtRecEvent, evtRecTrkCol)) continue;

		Gam_Track.push_back(i);

		EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
		RecEmcShower *emcTrk = (*itTrk)->emcShower();

		Hep3Vector Gm_Vec(emcTrk->x(), emcTrk->y(), emcTrk->z());
		Hep3Vector Gm_Mom = Gm_Vec - xorigin;
		Gm_Mom.setMag(emcTrk->energy());
		HepLorentzVector pGam(Gm_Mom, emcTrk->energy());

		Gam_EP.push_back(pGam);
		double px = pGam.px();
		double py = pGam.py();
		double pz = pGam.pz();
		double e  = pGam.e();
		double index = double(i);
		vector<double> tem;
		tem.push_back(px);
		tem.push_back(py);
		tem.push_back(pz);
		tem.push_back(e);
		tem.push_back(index);
		Gamma.push_back(tem);
	}


}	

bool isGoodShower(int i,SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol){

	EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
	if(!(*itTrk)->isEmcShowerValid()) return false;

	Hep3Vector xorigin(0,0,0);
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex();
		double*  vv = vtxsvc->SigmaPrimaryVertex();
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		return false;
	}

	RecEmcShower *emcTrk = (*itTrk)->emcShower();
	Hep3Vector Gm_Vec(emcTrk->x(), emcTrk->y(), emcTrk->z());
	Hep3Vector Gm_Mom = Gm_Vec - xorigin;
	Gm_Mom.setMag(emcTrk->energy());
	HepLorentzVector Gam_EP(Gm_Mom, emcTrk->energy());

	double detecttime=emcTrk->time();
	if(!(detecttime<14.0)) return false;//time cut

	double eraw = emcTrk->energy();
	double costheta=Gam_EP.vect().cosTheta();

	if(fabs(costheta)<0.8)
		if(!(eraw>0.025)) return false;
	if(0.84<fabs(costheta)&&fabs(costheta)<0.92)
		if(!(eraw>0.05)) return false;

	return true;
}

void Lambdac::IsPi0(SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vp4 Gam_EP,Vint Gam_Track,vector<PI0>& Pi0_class,vector<vector<double> >& Pi0){

	bool isGoodpi0(RecEmcShower * shr1,RecEmcShower * shr2,HepLorentzVector & );
	int nGam=Gam_Track.size();

	for(int i = 0; i < nGam-1; i++) 
	{
		for(int j = i+1; j < nGam; j++) 
		{
			HepLorentzVector Pi0_ep;
			Pi0_ep=Gam_EP[i] + Gam_EP[j];
			double pi0_mass=Pi0_ep.m();
			if(!(0.115<pi0_mass && pi0_mass< 0.150))  continue;

			EvtRecTrackIterator itTrki = evtRecTrkCol->begin() + Gam_Track[i];
			RecEmcShower* shr1 = (*itTrki)->emcShower();
			EvtRecTrackIterator itTrkj = evtRecTrkCol->begin() + Gam_Track[j];
			RecEmcShower* shr2 = (*itTrkj)->emcShower();

			HepLorentzVector p4_pi0(0,0,0,0);

			if(isGoodpi0(shr1,shr2,p4_pi0)){
				TLorentzVector Mgam1(Gam_EP[i].px(),
									 Gam_EP[i].py(),
									 Gam_EP[i].pz(),
									 Gam_EP[i].e());
				TLorentzVector Mgam2(Gam_EP[j].px(),
									 Gam_EP[j].py(),
									 Gam_EP[j].pz(),
									 Gam_EP[j].e());	
				
				TLorentzVector* Mpi0 = new TLorentzVector(p4_pi0.px(),
														p4_pi0.py(),
														p4_pi0.pz(),
														p4_pi0.e());
			
				PI0 Pi0Track(Mgam1,Mgam2,*Mpi0,Gam_Track[i],Gam_Track[j]);
				Pi0_class.push_back(Pi0Track);
		
				vector<double> tem;
				tem.push_back(p4_pi0.px());
				tem.push_back(p4_pi0.py());
				tem.push_back(p4_pi0.pz());
				tem.push_back(p4_pi0.e());
				tem.push_back(double(Gam_Track[i]));
				tem.push_back(double(Gam_Track[j]));
				Pi0.push_back(tem);

			}

		}
	}
}

bool isGoodpi0(RecEmcShower * shr1,RecEmcShower * shr2,HepLorentzVector & p4_pi0){

	double pi0_chis=-100;
	double xmpi0=0.134976;

	KalmanKinematicFit * kmfit = KalmanKinematicFit::instance();
	kmfit->init();
	kmfit->setIterNumber(5);
	kmfit->AddTrack(0, 0.0, shr1);
	kmfit->AddTrack(1, 0.0, shr2);
	kmfit->AddResonance(0, xmpi0, 0, 1);

	bool olmdq =kmfit->Fit(0);
	if(!olmdq) return false;

	kmfit->BuildVirtualParticle(0);
	pi0_chis = kmfit->chisq(0);

	p4_pi0=kmfit->pfit(0)+kmfit->pfit(1);

	return true;
}

void Lambdac::IsGoodTrack(SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,double z0_cut,double r0_cut,Vint & iGood,vector<vector<double> >& AllTracks ){

	Hep3Vector xorigin(0,0,0);

	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(vtxsvc->isVertexValid()){
		double* dbv = vtxsvc->PrimaryVertex(); 
		double*  vv = vtxsvc->SigmaPrimaryVertex();  
		xorigin.setX(dbv[0]);
		xorigin.setY(dbv[1]);
		xorigin.setZ(dbv[2]);
	}else{
		cout<<"invalid vertex"<<endl;
		return;
	}

	for(int i = 0; i < evtRecEvent->totalCharged(); i++){
		EvtRecTrackIterator itTrk=evtRecTrkCol->begin() + i;
		if(!(*itTrk)->isMdcTrackValid()) continue;

		RecMdcTrack *mdcTrk = (*itTrk)->mdcTrack();
		
		HepVector a = mdcTrk->helix();
		HepSymMatrix Ea = mdcTrk->err();
		HepPoint3D point0(0.,0.,0.);   // the initial point for MDC recosntruction
		HepPoint3D IP(xorigin[0],xorigin[1],xorigin[2]); 
		VFHelix helixip(point0,a,Ea); 
		helixip.pivot(IP);
		HepVector vecipa = helixip.a();
		double  Rvxy0=fabs(vecipa[0]);  //the nearest distance to IP in xy plane
		double  Rvz0=vecipa[3];         //the nearest distance to IP in z direction

		double costheta = cos( mdcTrk->theta() );
		if(!(abs(costheta)<0.93)) continue;

		if(z0_cut>0){
			if(fabs(Rvz0) >= z0_cut) continue;
		}

		if(r0_cut>0){
			if(fabs(Rvxy0) >= r0_cut) continue;
		}

		if((*itTrk)->isMdcKalTrackValid()){
			RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();
			double px = mdcKalTrk->px();
			double py = mdcKalTrk->py();
			double pz = mdcKalTrk->pz();
			vector<double> temp;
			temp.push_back(px);
			temp.push_back(py);
			temp.push_back(pz);
			temp.push_back(double(i));
			AllTracks.push_back(temp);
		}

		iGood.push_back(i);
	}


}

void Lambdac::IsPionM(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Pion_EP,Vint & Pion_Track,vector<vector<double> >& PionM){

	ParticleID * pid=ParticleID::instance();
	double m_prob_pid_pi,m_prob_pid_k;

	for(int i = 0; i < Track_10.size(); i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,pion_ID,4122)||matchSvc->match(*itTrk,pion_ID,-4122))){
				continue;
			}
		}


		if(open==1){

			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_pi>0&&m_prob_pid_pi>m_prob_pid_k))
				continue;
		}else if(open==2){
		
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyProton()|pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();
			double pro_proton = pid->probProton();

			if(!(m_prob_pid_pi>0&&m_prob_pid_pi>m_prob_pid_k&&m_prob_pid_pi>pro_proton))
				continue;
		
		}	

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();
		mdcKalTrk->setPidType(RecMdcKalTrack::pion);
		if(!(mdcKalTrk->charge()==-1)) continue;

		HepLorentzVector ptrk;
		ptrk.setPx(mdcKalTrk->px());
		ptrk.setPy(mdcKalTrk->py());
		ptrk.setPz(mdcKalTrk->pz());
		double p3 = ptrk.mag();
		ptrk.setE(sqrt(p3*p3+mpi*mpi));
		Pion_EP.push_back(ptrk);
		Pion_Track.push_back(Track_10[i]);

		vector<double> tem;
		tem.push_back(mdcKalTrk->px());
		tem.push_back(mdcKalTrk->py());
		tem.push_back(mdcKalTrk->pz());
		tem.push_back(sqrt(p3*p3+mpi*mpi));
		tem.push_back(double(Track_10[i]));
		PionM.push_back(tem);

	}

}

void Lambdac::IsPionP(int open,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint Track_10,Vp4 & Pion_EP,Vint & Pion_Track,vector< vector<double> >& PionP){

	ParticleID * pid=ParticleID::instance();
	double m_prob_pid_pi,m_prob_pid_k;

	for(int i = 0; i < Track_10.size(); i++) {

		EvtRecTrackIterator itTrk = evtRecTrkCol->begin() + Track_10[i]; 
		if(!(*itTrk)->isMdcKalTrackValid()) continue;

		if(matchSwitch=="1"&&runNumber<0){
			if(!(matchSvc->match(*itTrk,pion_ID,4122)||matchSvc->match(*itTrk,pion_ID,-4122))){
				continue;
			}
		}

		if(open==1){
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();

			if(!(m_prob_pid_pi>0&&m_prob_pid_pi>m_prob_pid_k))
				continue;
		}else if(open==2){
		
			pid->init();
			pid->setMethod(pid->methodProbability());
			pid->setChiMinCut(4);
			pid->setRecTrack(*itTrk);
			pid->usePidSys(pid->useDedx() | pid->useTof1() | pid->useTof2() | pid->useTofE()); // use PID sub-system
			pid->identify(pid->onlyProton()|pid->onlyPion() | pid->onlyKaon());    // seperater Pion/Kaon
			pid->calculate();

			if(!(pid->IsPidInfoValid())) continue;

			m_prob_pid_pi = pid->probPion();
			m_prob_pid_k  = pid->probKaon();
			double pro_proton = pid->probProton();

			if(!(m_prob_pid_pi>0&&m_prob_pid_pi>m_prob_pid_k&&m_prob_pid_pi>pro_proton))
				continue;
		
		}	

		RecMdcKalTrack* mdcKalTrk = (*itTrk)->mdcKalTrack();
		mdcKalTrk->setPidType(RecMdcKalTrack::pion);
		if(!(mdcKalTrk->charge()==1)) continue;

		HepLorentzVector ptrk;
		ptrk.setPx(mdcKalTrk->px());
		ptrk.setPy(mdcKalTrk->py());
		ptrk.setPz(mdcKalTrk->pz());
		double p3 = ptrk.mag();
		ptrk.setE(sqrt(p3*p3+mpi*mpi));
		Pion_EP.push_back(ptrk);
		Pion_Track.push_back(Track_10[i]);

		vector<double> tem;
		tem.push_back(mdcKalTrk->px());
		tem.push_back(mdcKalTrk->py());
		tem.push_back(mdcKalTrk->pz());
		tem.push_back(sqrt(p3*p3+mpi*mpi));
		tem.push_back(double(Track_10[i]));
		PionP.push_back(tem);
	}


}


void Lambdac::IsLbd(SmartDataPtr<EvtRecEvent> evtRecEvent,SmartDataPtr<EvtRecTrackCol> evtRecTrkCol,Vint iprotonp_loose,Vint iprotonm_loose,Vint ipionm_loose,Vint ipionp_loose,vector<LBD>& Lbd_class,vector<LBD>& ALbd_class,vector<vector<double> >& Lbd,vector<vector<double> >& ALbd){

	bool isGoodlmd(RecMdcKalTrack* ppTrk, RecMdcKalTrack* pimTrk,  HepLorentzVector& p4_lmd_1s);

	for(int i = 0; i < iprotonp_loose.size(); i++) 
	{
		for(int j = 0; j < ipionm_loose.size(); j++) 
		{
			EvtRecTrackIterator itTrki = evtRecTrkCol->begin() + iprotonp_loose[i];
			RecMdcKalTrack *ppKalTrk = (*itTrki)->mdcKalTrack();

			EvtRecTrackIterator itTrkj = evtRecTrkCol->begin() + ipionm_loose[j];
			RecMdcKalTrack *pimKalTrk = (*itTrkj)->mdcKalTrack();

			HepLorentzVector p4_lmd_1s(0,0,0,0);

			if(isGoodlmd(ppKalTrk, pimKalTrk,p4_lmd_1s) ){
				double e_pp=sqrt(ppKalTrk->px()*ppKalTrk->px()+
								 ppKalTrk->py()*ppKalTrk->py()+
								 ppKalTrk->pz()*ppKalTrk->pz()+
								 mp*mp);
			    double e_pim=sqrt(pimKalTrk->px()*pimKalTrk->px()+
								 pimKalTrk->py()*pimKalTrk->py()+
								 pimKalTrk->pz()*pimKalTrk->pz()+
								 mpi*mpi);


				TLorentzVector mpp(ppKalTrk->px(),
								   ppKalTrk->py(),
								   ppKalTrk->pz(),
								   e_pp);
				TLorentzVector mpim(pimKalTrk->px(),
								    pimKalTrk->py(),
									pimKalTrk->pz(),
									e_pim);

				TLorentzVector* mlbd=new TLorentzVector(p4_lmd_1s.px(),
														p4_lmd_1s.py(),
														p4_lmd_1s.pz(),
														p4_lmd_1s.e());

				vector<double> tem;
				tem.push_back(p4_lmd_1s.px());
				tem.push_back(p4_lmd_1s.py());
				tem.push_back(p4_lmd_1s.pz());
				tem.push_back(p4_lmd_1s.e());
				tem.push_back(double(iprotonp_loose[i]));
				tem.push_back(double(ipionm_loose[j]));
				Lbd.push_back(tem);

				LBD TrackLbd(mpp,mpim,*mlbd,iprotonp_loose[i],ipionm_loose[j]);
				Lbd_class.push_back(TrackLbd);
			
			
			
			}

		}
	}

	// Loop each pbar pi+ pair, check if it is a Anti-lambda
	for(int i = 0; i < iprotonm_loose.size(); i++) 
	{
		for(int j = 0; j < ipionp_loose.size(); j++) 
		{
			EvtRecTrackIterator itTrki = evtRecTrkCol->begin() + iprotonm_loose[i];
			RecMdcKalTrack *pmKalTrk = (*itTrki)->mdcKalTrack();

			EvtRecTrackIterator itTrkj = evtRecTrkCol->begin() + ipionp_loose[j];
			RecMdcKalTrack *pipKalTrk = (*itTrkj)->mdcKalTrack();

			HepLorentzVector p4_lmd_1s(0,0,0,0);

			if(isGoodlmd(pmKalTrk, pipKalTrk, p4_lmd_1s)){
				double e_pm=sqrt(pmKalTrk->px()*pmKalTrk->px()+
								 pmKalTrk->py()*pmKalTrk->py()+
								 pmKalTrk->pz()*pmKalTrk->pz()+
								 mp*mp);
			    double e_pip=sqrt(pipKalTrk->px()*pipKalTrk->px()+
								 pipKalTrk->py()*pipKalTrk->py()+
								 pipKalTrk->pz()*pipKalTrk->pz()+
								 mpi*mpi);

				TLorentzVector mpm(pmKalTrk->px(),
								   pmKalTrk->py(),
								   pmKalTrk->pz(),
								   e_pm);
				TLorentzVector mpip(pipKalTrk->px(),
								    pipKalTrk->py(),
									pipKalTrk->pz(),
									e_pip);
				
				TLorentzVector* mlbd=new TLorentzVector(p4_lmd_1s.px(),
														p4_lmd_1s.py(),
														p4_lmd_1s.pz(),
														p4_lmd_1s.e());
			
				LBD TrackLbd(mpm,mpip,*mlbd,iprotonm_loose[i],ipionp_loose[j]);
				ALbd_class.push_back(TrackLbd);
	
				vector<double> tem;
				tem.push_back(p4_lmd_1s.px());
				tem.push_back(p4_lmd_1s.py());
				tem.push_back(p4_lmd_1s.pz());
				tem.push_back(p4_lmd_1s.e());
				tem.push_back(double(iprotonm_loose[i]));
				tem.push_back(double(ipionp_loose[j]));
				ALbd.push_back(tem);
			
			}
		}
	}
}

bool isGoodlmd(RecMdcKalTrack* ppTrk, RecMdcKalTrack* pimTrk,  HepLorentzVector& p4_lmd_1s){

	double lmd_1chis=-100;
	double lmd_2chis=-100;
	double lmd_lchue =-100;
	double lmd_mass =-100;

	HepPoint3D vx(0., 0., 0.);
	HepSymMatrix Evx(3, 0);
	double bx = 1E+6;
	double by = 1E+6;
	double bz = 1E+6;
	Evx[0][0] = bx*bx;
	Evx[1][1] = by*by;
	Evx[2][2] = bz*bz;
	VertexParameter vxpar;
	vxpar.setVx(vx);
	vxpar.setEvx(Evx);

	VertexFit *vtxfit_s = VertexFit::instance();
	SecondVertexFit *vtxfit2 = SecondVertexFit::instance();

	WTrackParameter  wvpplmdTrk, wvpimlmdTrk;

	RecMdcKalTrack::setPidType(RecMdcKalTrack::proton);
	wvpplmdTrk = WTrackParameter(mproton, ppTrk->getZHelix(),ppTrk->getZError());

	RecMdcKalTrack::setPidType(RecMdcKalTrack::pion);
	wvpimlmdTrk = WTrackParameter(mpion, pimTrk->getZHelix(),pimTrk->getZError());

	//******primary vertex fit
	vtxfit_s->init();
	vtxfit_s->AddTrack(0, wvpplmdTrk);
	vtxfit_s->AddTrack(1, wvpimlmdTrk);
	vtxfit_s->AddVertex(0, vxpar, 0, 1);
	bool okvs=vtxfit_s->Fit(0);

	if(!okvs) return false;

	vtxfit_s->Swim(0);
	vtxfit_s->BuildVirtualParticle(0);
	lmd_1chis = vtxfit_s->chisq(0);

	if(!(lmd_1chis<100)) return false;

	WTrackParameter  wvlmd = vtxfit_s->wVirtualTrack(0);
	p4_lmd_1s=wvlmd.p();

	lmd_mass = p4_lmd_1s.m();

	if(!(lmd_mass > 1.111 && lmd_mass< 1.121)) return false;

	WTrackParameter wppFlmd = vtxfit_s->wtrk(0);
	WTrackParameter wpimFlmd = vtxfit_s->wtrk(1);
	//******second vertex fit
	HepPoint3D newvx(0., 0., 0.);
	HepSymMatrix newEvx(3, 0);
	VertexParameter primaryVpar;
	IVertexDbSvc*  vtxsvc;
	Gaudi::svcLocator()->service("VertexDbSvc", vtxsvc);
	if(!(vtxsvc->isVertexValid())) {
		cout<<"Attention --!(vtxsvc->isVertexValid())"<<endl;
		return false;
	}
	double* db_vx = vtxsvc->PrimaryVertex();
	double* db_vx_err = vtxsvc->SigmaPrimaryVertex();
	newvx.setX(db_vx[0]);
	newvx.setY(db_vx[1]);
	newvx.setZ(db_vx[2]);
	newEvx[0][0] = db_vx_err[0]*db_vx_err[0];
	newEvx[1][1] = db_vx_err[1]*db_vx_err[1];
	newEvx[2][2] = db_vx_err[2]*db_vx_err[2];
	primaryVpar.setVx(newvx);
	primaryVpar.setEvx(newEvx);
	vtxfit2->init();
	vtxfit2->setPrimaryVertex(primaryVpar);
	vtxfit2->AddTrack(0, wvlmd);
	vtxfit2->setVpar(vtxfit_s->vpar(0));
	bool okv2=vtxfit2->Fit();
	if(!okv2) return false;

	lmd_2chis= vtxfit2->chisq();

	double lmd_dl  = vtxfit2->decayLength();
	double lmd_dle = vtxfit2->decayLengthError();
	lmd_lchue = lmd_dl/lmd_dle;

	if(!(lmd_lchue>2)) return false;

	return true;
}


