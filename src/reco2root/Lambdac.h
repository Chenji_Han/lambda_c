/*************************************************************************
    > File Name: Lambdac.h
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 16 Aug 2018 10:36:13 AM CST
 ************************************************************************/
#ifndef Lambdac_H
#define Lambdac_H 

#include<fstream>
#include"McTruth/McParticle.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/Algorithm.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/AlgFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/PropertyMgr.h"
#include "VertexFit/IVertexDbSvc.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"

#include "EventModel/EventModel.h"
#include "EventModel/Event.h"

#include "EvtRecEvent/EvtRecEvent.h"
#include "EvtRecEvent/EvtRecTrack.h"
#include "DstEvent/TofHitStatus.h"
#include "EventModel/EventHeader.h"

#include "VertexFit/Helix.h"
#include "VertexFit/SecondVertexFit.h"
#include "VertexFit/KalmanKinematicFit.h"

#include "TMath.h"
#include "GaudiKernel/INTupleSvc.h"
#include "GaudiKernel/NTuple.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "CLHEP/Vector/ThreeVector.h"
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Vector/TwoVector.h"

#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"
#include <vector>

//#include "VertexFit/KinematicFit.h"
#include "VertexFit/KalmanKinematicFit.h"
#include "VertexFit/VertexFit.h"
#include "VertexFit/Helix.h"
#include "ParticleID/ParticleID.h"

#include "MCTruthMatchSvc/MCTruthMatchSvc.h"

#include "TLorentzVector.h"
#include <TFile.h>
#include <TTree.h>
#include "Tracks_class.h"
#include "TClonesArray.h"

using CLHEP::Hep3Vector;
using CLHEP::Hep2Vector;
using CLHEP::HepLorentzVector;
#include "CLHEP/Geometry/Point3D.h"
#ifndef ENABLE_BACKWARDS_COMPATIBILITY
   typedef HepGeom::Point3D<double> HepPoint3D;
#endif
typedef std::vector<int> Vint;
typedef std::vector<HepLorentzVector> Vp4;

const double mpion    = 0.13957;
const double mpi      = 0.13957;
const double mk       = 0.493677;
const double mp       = 0.938272;
const double mproton  = 0.938272;

class Lambdac : public Algorithm {

	public:
	
	Lambdac(const std::string& name, ISvcLocator* pSvcLocator);
	StatusCode initialize();
  	
	StatusCode execute();

  	StatusCode finalize();  

	void IsSigma0(vector<LBD>,vector<LBD>,Vp4,Vint,vector<vector<double> >&,vector<vector<double> >&);
	
	void IsLbd(SmartDataPtr<EvtRecEvent>,SmartDataPtr<EvtRecTrackCol>,Vint,Vint,Vint,Vint,vector<LBD>&,vector<LBD>&,vector<vector<double> >&,vector<vector<double> >&);
	
	void IsPionP(int,SmartDataPtr<EvtRecTrackCol>,Vint,Vp4&,Vint&,vector<vector<double> >&);
	
	void IsPionM(int,SmartDataPtr<EvtRecTrackCol>,Vint,Vp4&,Vint&,vector<vector<double> >&);
	
	void IsGoodTrack(SmartDataPtr<EvtRecEvent>,SmartDataPtr<EvtRecTrackCol>,double,double,Vint&,vector<vector<double> >&);
	
	void IsPi0(SmartDataPtr<EvtRecTrackCol>l,Vp4,Vint,vector<PI0>&,vector<vector<double> >&);
	
	void IsGammatrack(SmartDataPtr<EvtRecEvent>,SmartDataPtr<EvtRecTrackCol>,Vint&,Vp4&,vector<vector<double> >&);
	
	void IsProtonP(int,SmartDataPtr<EvtRecTrackCol> ,Vint,Vp4 &,Vint &,vector<vector<double> >&);
	
	void IsProtonM(int,SmartDataPtr<EvtRecTrackCol> ,Vint,Vp4 &,Vint &,vector<vector<double> >&);
	
	void IsKaonM(int,SmartDataPtr<EvtRecTrackCol>,Vint,Vp4&,Vint&,vector<vector<double> >&);
	
	void IsKaonP(int,SmartDataPtr<EvtRecTrackCol>,Vint,Vp4&,Vint&,vector<vector<double> >&);
	
	void IsKs0(SmartDataPtr<EvtRecTrackCol>,Vint,Vint,vector<vector<double> >&);
	
	void IsSigmap(SmartDataPtr<EvtRecTrackCol>,Vp4,Vint,vector<PI0>,vector<vector<double> >&);
	
	void IsSigmam(SmartDataPtr<EvtRecTrackCol>,Vp4,Vint,vector<PI0>,vector<vector<double> >&);
	
	void IsXi(SmartDataPtr<EvtRecTrackCol>,Vint,Vint,vector<vector<double> >&);

	private:

  TFile* outputFile;
  TTree* outputTree;

  int runNumber, eventNumber;

  vector<vector<double> > AllTracks;
  
  vector<vector<double> > ProtonP;
  vector<vector<double> > ProtonM;
  vector<vector<double> > PionP;
  vector<vector<double> > PionM;
  vector<vector<double> > KaonP;
  vector<vector<double> > KaonM;
  vector<vector<double> > Gamma;

  vector<vector<double> > Pi0;
  vector<vector<double> > Ks0;
  vector<vector<double> > Xi;
  vector<vector<double> > XiBar;
  vector<vector<double> > Lbd;
  vector<vector<double> > ALbd;
  vector<vector<double> > Sigma0;
  vector<vector<double> > ASigma0;
  vector<vector<double> > SigmaP;
  vector<vector<double> > SigmaM;

  MCTruthMatchSvc * matchSvc;
  string matchSwitch;	

    
};






#endif 






