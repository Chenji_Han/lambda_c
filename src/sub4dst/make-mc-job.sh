#!/bin/bash
set +x

ecms=$1

#pthe data file
dpath1=/besfs3/offline/data/703-1/xyz/round07/4600/dst/$ecms

dataFiles=`ls $dpath1/**dst `
fileNumber=`ls $dpath1/**dst | wc -l`

##the data file number in one job
let nDataFile=5

##the job name with the format $myjob$iJob
let NUM=1
iJob=`expr 100000 + $NUM | cut -c 4-6`
myjob=${ecms}-

mkdir ${ecms}
cd ${ecms}
touch sub.sh
##change something below ONLY when you know what you are dong
let n=0
for recFile in $dataFiles
do 

  #echo $recFile
  let m=n+1

  if [[ ! -e $recFile ]]; then 
    echo "this data file $recFile doesn't exist! "
  else
 
    line="\"$recFile\""

    if [[ $n%$nDataFile -eq 0 ]]; then
	  if [[ -e dst_${myjob}${iJob} ]]; then
          cat ../../job.foot >> dst_${myjob}${iJob}
		  echo "dst_outputName ../${myjob}${iJob}.root" >> inputs.txt 
		  echo "dst_matchSwitch 0" >> inputs.txt
		  echo "cd ${myjob}${iJob}" >> ../sub.sh
          echo "boss.condor ./dst_${myjob}${iJob}" >> ../sub.sh
		  echo "cd .." >> ../sub.sh
          let NUM++
          iJob=`expr 100000 + $NUM | cut -c 4-6`
		  cd ../
	  fi
      mkdir  ${myjob}${iJob}
      cd  ${myjob}${iJob}
	  touch dst_${myjob}${iJob}
	  touch inputs.txt
	  cat ../../job.head >> dst_${myjob}${iJob}
    fi

    if [[ $m%$nDataFile -eq 0 || $m -eq $fileNumber ]]; then
      echo ${line} >> dst_${myjob}${iJob}
    else
      echo ${line}, >> dst_${myjob}${iJob}
    fi


    let n++
  fi
done

##for the last jobOption file
cat  ../../job.foot >> dst_${myjob}${iJob}
echo "dst_outputName ../${myjob}${iJob}.root" >> inputs.txt 
echo "dst_matchSwitch 0" >> inputs.txt	
echo "cd ${myjob}${iJob}" >> ../sub.sh
echo "boss.condor dst_${myjob}${iJob}" >> ../sub.sh
echo "cd .." >> ../sub.sh

chmod +x ../sub.sh
cd ../..

echo "cd ${ecms}" >> sub.sh
echo "./sub.sh" >> sub.sh
echo "cd .." >> sub.sh
echo "hadd ${ecms}.root ${ecms}/*.root" >> merge.sh

exit 0
