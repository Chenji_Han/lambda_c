#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooAbsData.h"
#include "RooGaussian.h"
#include "RooLandau.h"
#include "RooFFTConvPdf.h"
#include "RooPlot.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TAxis.h"
#include "TH1.h"

#include"bes3plotstyle.C"
#include "/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/src/Fit_combine.C"

using namespace RooFit ;
void plot_combined_4_4()
{

//	SetStyle();
  gROOT->SetStyle("Plain");
  gStyle->SetOptTitle(0);
  gStyle->SetLabelFont(22,"xyz");
  gStyle->SetLabelSize(0.085,"xyz");
  gStyle->SetNdivisions(510,"xyz");
  gStyle->SetTitleFont(22,"xyz");
  //gStyle->SetTitleSize(0.09,"xyz");
  gStyle->SetTitleSize(0.,"xyz");
  gStyle->SetTitleOffset(1.0,"x");
  gStyle->SetTitleOffset(1.4,"y");
  gStyle->SetMarkerStyle(20);
  gStyle->SetMarkerSize(0.03);
  //gStyle->SetFrameLineWidth(2);
  gStyle->SetFrameLineWidth(0.005);
  gStyle->SetFrameFillColor(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  gStyle->SetTickLength(0.05);

   // No Canvas Border
  gStyle->SetCanvasBorderMode(0);
  gStyle->SetCanvasBorderSize(0);
  // No pad borders
  gStyle->SetPadBorderMode(0);
  gStyle->SetPadBorderSize(0);
  // Margins for labels etc.
  gStyle->SetPadLeftMargin(0.25);
  gStyle->SetPadBottomMargin(0.20);
  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadTopMargin(0.05);


//////////////////////////////////////////////////////
//TLatex *latex0 = new TLatex(0.45,0.030,"E_{sum}^{extra #gamma} (GeV)");
TLatex *latex0 = new TLatex(0.45,0.02,"M_{BC} (GeV/c^{2})");
 TLatex *latex01 = new TLatex(0.02,0.38,"Events");
// TLatex *latex01 = new TLatex(0.03,0.38,"Events / (100 MeV)");
  TLatex *latex1 = new TLatex(0.80,0.80,"a)");
  TLatex *latex2 = new TLatex(0.80,0.80,"b)");
  TLatex *latex3 = new TLatex(0.80,0.80,"c)");
  TLatex *latex4 = new TLatex(0.80,0.80,"d)");
  TLatex *latex5 = new TLatex(0.80,0.80,"e)");
  TLatex *latex6 = new TLatex(0.80,0.80,"f)");
  TLatex *latex7 = new TLatex(0.80,0.80,"g)");
  TLatex *latex8 = new TLatex(0.80,0.80,"h)");
  TLatex *latex9 = new TLatex(0.80,0.80,"i)");
  TLatex *latex10 = new TLatex(0.80,0.80,"j)");
  latex0->SetNDC();
  latex01->SetNDC();
  latex1->SetNDC();
  latex2->SetNDC();
  latex3->SetNDC();
  latex4->SetNDC();
  latex5->SetNDC();
  latex6->SetNDC();
  latex7->SetNDC();
  latex8->SetNDC();
  latex9->SetNDC();
  latex10->SetNDC();
  latex0->SetTextFont(22);
  latex01->SetTextFont(22);
  latex1->SetTextFont(72);
  latex2->SetTextFont(72);
  latex3->SetTextFont(72);
  latex4->SetTextFont(72);
  latex5->SetTextFont(72);
  latex6->SetTextFont(72);
  latex7->SetTextFont(72);
  latex8->SetTextFont(72);
  latex9->SetTextFont(72);
  latex10->SetTextFont(72);
  latex0->SetTextSize(0.04);
  latex01->SetTextSize(0.04);
  latex1->SetTextSize(0.1);
  latex2->SetTextSize(0.1);
  latex3->SetTextSize(0.1);
  latex4->SetTextSize(0.1);
  latex5->SetTextSize(0.1);
  latex6->SetTextSize(0.1);
  latex7->SetTextSize(0.1);
  latex8->SetTextSize(0.1);
  latex9->SetTextSize(0.1);
  latex10->SetTextSize(0.1);

latex01->SetTextAngle(90);
//TCanvas *c1 = new TCanvas("c1", "c1",0,0,640,800);
TCanvas *c1 = new TCanvas("c1", "c1",0,0,900,600); 

// c1->Range(0,0,1,1);
  c1->SetBorderSize(2);
  c1->SetLeftMargin(0.5);
  c1->SetRightMargin(0.02);
  c1->SetTopMargin(0.10);
  c1->SetBottomMargin(0.05);
//latex0->Draw();
//TPad *c1_1  = new TPad("c1_1",  "c1_1", 0.00,0.60,0.25,1.0);
//TPad *c1_2  = new TPad("c1_2",  "c1_2", 0.25,0.60,0.5, 1.0);
//TPad *c1_3  = new TPad("c1_3",  "c1_3", 0.5, 0.60,0.75,1.0);
//TPad *c1_4  = new TPad("c1_4",  "c1_4", 0.75,0.60,1.0, 1.0);
//TPad *c1_5  = new TPad("c1_5",  "c1_5", 0.00,0.30,0.25,0.70);
//TPad *c1_6  = new TPad("c1_6",  "c1_6", 0.25,0.30,0.5, 0.70);
//TPad *c1_7  = new TPad("c1_7",  "c1_7", 0.5, 0.30,0.75,0.70);
//TPad *c1_8  = new TPad("c1_8",  "c1_8", 0.75,0.30,1.0, 0.70);
//TPad *c1_9  = new TPad("c1_9",  "c1_9", 0.00,0.0, 0.25,0.40);
//TPad *c1_10 = new TPad("c1_10", "c1_10",0.25,0.0, 0.5, 0.4);


TPad *c1_1  = new TPad("c1_1",  "c1_1", 0.02,0.69,0.28,0.99);
TPad *c1_2  = new TPad("c1_2",  "c1_2", 0.26,0.69,0.52, 0.99);
TPad *c1_3  = new TPad("c1_3",  "c1_3", 0.51, 0.69,0.77,0.99);
TPad *c1_4  = new TPad("c1_4",  "c1_4", 0.75,0.69,1.0,0.99);

TPad *c1_5  = new TPad("c1_5",  "c1_5", 0.02,0.39,0.28,0.70);
TPad *c1_6  = new TPad("c1_6",  "c1_6", 0.26,0.39,0.52, 0.70);
TPad *c1_7  = new TPad("c1_7",  "c1_7", 0.51, 0.39,0.77,0.70);
TPad *c1_8  = new TPad("c1_8",  "c1_8", 0.75,0.39,1.0, 0.70);

TPad *c1_9  = new TPad("c1_9",  "c1_9",  0.02,0.09,0.28,0.40);
TPad *c1_10 = new TPad("c1_10", "c1_10", 0.26,0.09,0.52, 0.40);
TPad *c1_11 = new TPad("c1_11", "c1_11", 0.51,0.09,0.77,0.40);
TPad *c1_12 = new TPad("c1_12", "c1_12", 0.75,0.09,1.0, 0.40);

//c1->cd();
  c1_1->Draw();
  c1_1->cd();
  c1_1->Range(0,0,1,1);
  c1_1->SetFillStyle(4000);
  c1_1->SetBorderMode(0);
  c1_1->SetBorderSize(2);
  c1_1->SetTickx();
  c1_1->SetTicky();
  c1_1->SetLeftMargin(0.25);
  c1_1->SetRightMargin(0.02);
  c1_1->SetTopMargin(0.02);
  c1_1->SetBottomMargin(0.10);
  c1_1->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/LbdPi/ST/inputs.txt");
	c1_1->Modified();
	c1->cd();

//////////////////////////////////////////////
  c1_2->Draw();
  c1_2->cd();
  c1_2->Range(0,0,1,1);
  c1_2->SetFillStyle(4000);
  c1_2->SetBorderMode(0);
  c1_2->SetBorderSize(2);
  c1_2->SetTickx();
  c1_2->SetTicky();
 
  c1_2->SetLeftMargin(0.25);
  c1_2->SetRightMargin(0.02);
  c1_2->SetTopMargin(0.02);
  c1_2->SetBottomMargin(0.10);
  c1_2->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/LbdPiPi0/ST/inputs.txt");
c1_2->Modified();
c1->cd();
////////////////////////////////////////////////////

  c1_3->Draw("");
  c1_3->cd();
  c1_3->Range(0,0,1,1);
  c1_3->SetFillStyle(4000);
  c1_3->SetBorderMode(0);
  c1_3->SetBorderSize(2);
  c1_3->SetTickx();
  c1_3->SetTicky();
  c1_3->SetLeftMargin(0.25);
  c1_3->SetRightMargin(0.02);
  c1_3->SetTopMargin(0.02);
  c1_3->SetBottomMargin(0.10);
  c1_3->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/Lbd3Pi/ST/inputs.txt");
//latex3->Draw();
c1_3->Modified();
c1->cd();
////////////////////////////////////////////////
  c1_4->Draw();
  c1_4->cd();
  c1_4->Range(0,0,1,1);
  c1_4->SetFillStyle(4000);
  c1_4->SetBorderMode(0);
  c1_4->SetBorderSize(2);
  c1_4->SetTickx();
  c1_4->SetTicky();

  c1_4->SetLeftMargin(0.25);
  c1_4->SetRightMargin(0.02);
  c1_4->SetTopMargin(0.02);
  c1_4->SetBottomMargin(0.10);
  c1_4->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/PKPi/ST/inputs.txt");
c1_4->Modified();
c1->cd();
///////////////////////////////////////////////
  c1_5->Draw();
  c1_5->cd();
  c1_5->Range(0,0,1,1);
  c1_5->SetFillStyle(4000);
  c1_5->SetBorderMode(0);
  c1_5->SetBorderSize(2);
  c1_5->SetTickx();
  c1_5->SetTicky();
	  
  c1_5->SetLeftMargin(0.25);
  c1_5->SetRightMargin(0.02);
  c1_5->SetTopMargin(0.02);
  c1_5->SetBottomMargin(0.10);
  c1_5->SetFrameFillColor(0);


  Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/PKPiPi0/ST/inputs.txt");
c1_5->Modified();
c1->cd();
/////////////////////////////////////////////////
  c1_6->Draw("");
  c1_6->cd();
  c1_6->Range(0,0,1,1);
  c1_6->SetFillStyle(4000);
  c1_6->SetBorderMode(0);
  c1_6->SetBorderSize(2);
  c1_6->SetTickx();
  c1_6->SetTicky();
  
  
  c1_6->SetLeftMargin(0.25);
  c1_6->SetRightMargin(0.02);
  c1_6->SetTopMargin(0.02);
  c1_6->SetBottomMargin(0.10);
  c1_6->SetFrameFillColor(0);


  Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/PKs/ST/inputs.txt");
//latex6->Draw();
c1_6->Modified();
c1->cd();
//////////////////////////////////////////////////

  c1_7->Draw("");
  c1_7->cd();
  c1_7->Range(0,0,1,1);
  c1_7->SetFillStyle(4000);
  c1_7->SetBorderMode(0);
  c1_7->SetBorderSize(2);
  c1_7->SetTickx();
  c1_7->SetTicky();
  c1_7->SetLeftMargin(0.25);
  c1_7->SetRightMargin(0.02);
  c1_7->SetTopMargin(0.02);
  c1_7->SetBottomMargin(0.10);
  c1_7->SetFrameFillColor(0);

	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/PKsPi0/ST/inputs.txt");
c1_7->Modified();
c1->cd();
////////////////////////////////////////////////////
  c1_8->Draw("");
  c1_8->cd();
  c1_8->Range(0,0,1,1);
  c1_8->SetFillStyle(4000);
  c1_8->SetBorderMode(0);
  c1_8->SetBorderSize(2);
  c1_8->SetTickx();
  c1_8->SetTicky();
	  
  c1_8->SetLeftMargin(0.25);
  c1_8->SetRightMargin(0.02);
  c1_8->SetTopMargin(0.02);
  c1_8->SetBottomMargin(0.10);
  c1_8->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/PKs2Pi/ST/inputs.txt");
c1_8->Modified();
c1->cd();
//////////////////////////////////////////////////////
  c1_9->Draw("");
  c1_9->cd();
  c1_9->Range(0,0,1,1);
  c1_9->SetFillStyle(4000);
  c1_9->SetBorderMode(0);
  c1_9->SetBorderSize(2);
  c1_9->SetTickx();
  c1_9->SetTicky();

  c1_9->SetLeftMargin(0.25);
  c1_9->SetRightMargin(0.02);
  c1_9->SetTopMargin(0.02);
  c1_9->SetBottomMargin(0.10);
  c1_9->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/P2Pi/ST/inputs.txt");
c1_9->Modified();
c1->cd();
//////////////////////////////////////////////////////

  c1_10->Draw("");
  c1_10->cd();
  c1_10->Range(0,0,1,1);
  c1_10->SetFillStyle(4000);
  c1_10->SetBorderMode(0);
  c1_10->SetBorderSize(2);
  c1_10->SetTickx();
  c1_10->SetTicky();
  
  c1_10->SetLeftMargin(0.25);
  c1_10->SetRightMargin(0.02);
  c1_10->SetTopMargin(0.02);
  c1_10->SetBottomMargin(0.10);
  c1_10->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/Sigma0Pi/ST/inputs.txt");
c1_10->Modified();
c1->cd();


  c1_11->Draw("");
  c1_11->cd();
  c1_11->Range(0,0,1,1);
  c1_11->SetFillStyle(4000);
  c1_11->SetBorderMode(0);
  c1_11->SetBorderSize(2);
  c1_11->SetTickx();
  c1_11->SetTicky();
 
  c1_11->SetLeftMargin(0.25);
  c1_11->SetRightMargin(0.02);
  c1_11->SetTopMargin(0.02);
  c1_11->SetBottomMargin(0.10);
  c1_11->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/SigmaPi0/ST/inputs.txt");
c1_10->Modified();
c1->cd();

  c1_12->Draw("");
  c1_12->cd();
  c1_12->Range(0,0,1,1);
  c1_12->SetFillStyle(4000);
  c1_12->SetBorderMode(0);
  c1_12->SetBorderSize(2);
  c1_12->SetTickx();
  c1_12->SetTicky();
 
  c1_12->SetLeftMargin(0.25);
  c1_12->SetRightMargin(0.02);
  c1_12->SetTopMargin(0.02);
  c1_12->SetBottomMargin(0.10);
  c1_12->SetFrameFillColor(0);


	Fit_combine("/afs/ihep.ac.cn/users/h/hancj/LambdacTagAlg/lambda_c/Sigma2Pi/ST/inputs.txt");
c1_12->Modified();
c1->cd();
latex0->Draw();
latex01->Draw();
c1->Print("Fit_Esum_4_4.eps");
c1->SaveAs("Fit_Esum_4_4.png");

}
