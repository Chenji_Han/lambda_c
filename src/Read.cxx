/*************************************************************************
    > File Name: read2.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Tue 25 Jun 2019 08:05:35 PM CST
 ************************************************************************/
#include"functions.cxx"
#include"TLorentzVector.h"
#include<vector>

void draw_sub(TH1D* Hist,string title,string XTitle,string saveName){

//	SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C;

	Hist -> GetXaxis() -> SetTitle(XTitle.c_str());
	Hist -> GetYaxis() -> SetTitle("events");
	Hist -> SetTitle(title.c_str());
	Hist -> Draw("hist");

	C.SaveAs(saveName.c_str());

}

void Read_sub(string inputName,string vblName,string vblTitle,vector<double> parameters){

	TFile* inputFile = new TFile(inputName.c_str());
	TTree* inputTree = (TTree*) inputFile -> Get("output");

	int nbins = parameters[0];
	double Xmin = parameters[1];
	double Xmax = parameters[2];
	TH1D* Hist = new TH1D(vblName.c_str(),"",nbins,Xmin,Xmax);

	vector<vector<double> >* Variable;
	inputTree -> SetBranchAddress(vblName.c_str(),&Variable);

	int evtNum = 0;

	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		if(ievt%10000==0) cout<<"processing..."<<ievt<<endl;
		inputTree ->GetEntry(ievt);

		if(!(Variable->size()>0)) continue;
		for(int index2=0;index2<1;index2++){
//		for(int index2=0;index2<Variables[index1]->size();index2++){
			vector<double> P4 = Variable -> at(index2);
			double M = sqrt(P4[3]*P4[3]-P4[0]*P4[0]-P4[1]*P4[1]-P4[2]*P4[2]);
			if(Xmin<M&&M<Xmax){
				evtNum++;
				Hist -> Fill(M);
			}
		}	
	}
		cout<<"the numbers of "<<vblName<<" are "<<evtNum<<endl;

//	ofstream mytxt;
//    mytxt.open("read_results.txt");
//	for(int i=0;i<cateNums;i++){
//		cout<<"the numbers of "<<vblNames[i]<<" are "<<evtNums[i]<<endl;
//		mytxt<<"the numbers of "<<vblNames[i]<<" are "<<evtNums[i]<<endl;
//	}
//	mytxt.close();

	draw_sub(Hist,"the invariant mass distribution of "+vblTitle,"Mass/GeV","read_"+vblName+".png");

	
}
void Read(){

	string inputName;
	vector<string> vblNames;
	vector<string> vblTitles;
	vector<vector<double> > vblParameters;

	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c,d,e,f;
        iss>>a>>b>>c>>d>>e>>f;

		if(a=="read_vbl"){
			vblNames.push_back(b);
			vblTitles.push_back(c);

			vector<double> parameters;
			parameters.push_back(string2double(d));
			parameters.push_back(string2double(e));
			parameters.push_back(string2double(f));
			vblParameters.push_back(parameters);
		}else if(a=="read_inputName"){
			inputName  = b;
		}
	}

	for(int i=0;i<vblNames.size();i++){
		Read_sub(inputName,vblNames[i],vblTitles[i],vblParameters[i]);
	}

}






















