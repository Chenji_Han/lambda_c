/*************************************************************************
    > File Name: PKs.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 02:35:54 AM CST
 ************************************************************************/

#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void PKs(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}


	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;
	vector<vector<double> >* Ks0_vector;
	vector<vector<double> >* ProtonP_vector;
	vector<vector<double> >* ProtonM_vector;
//	vector<vector<double> >* AllTracks;

//	inputTree -> SetBranchAddress("AllTracks",&AllTracks);
	inputTree -> SetBranchAddress("Ks0",&Ks0_vector);
	inputTree -> SetBranchAddress("ProtonP",&ProtonP_vector);
	inputTree -> SetBranchAddress("ProtonM",&ProtonM_vector);
	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_PKs_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;
	
	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);

	bool pass = false;
	int passNum = 0;
	
	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
//		if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;

		pass = false; dE =1.0; Mbc =0.0;
		int index[3];
		if(flag==1||flag==3){
		for(int i=0;i<Ks0_vector->size();i++){
			vector<double> Ks0_temp = Ks0_vector -> at(i);
			int Ks0_index1 = Ks0_temp[4]; 
			int Ks0_index2 = Ks0_temp[5]; 

			for(int j=0;j<ProtonP_vector->size();j++){
				vector<double> ProtonP_temp = ProtonP_vector -> at(j);
				int ProtonP_index = ProtonP_temp[4]; 
			
				if(ProtonP_index==Ks0_index1||ProtonP_index==Ks0_index2){
					continue;
				}

				TLorentzVector P4_total(Ks0_temp[0]+ProtonP_temp[0],
										Ks0_temp[1]+ProtonP_temp[1],
										Ks0_temp[2]+ProtonP_temp[2],
										Ks0_temp[3]+ProtonP_temp[3]);
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();

				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index[0]=Ks0_index1;
					index[1]=Ks0_index2;
					index[2]=ProtonP_index;
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			
			}//proton
		}//Ks0
		
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}

		}

		pass = false; dE =1.0; Mbc =0.0;
		if(flag==2||flag==3){
		for(int i=0;i<Ks0_vector->size();i++){
			vector<double> Ks0_temp = Ks0_vector -> at(i);
			int Ks0_index1 = Ks0_temp[4]; 
			int Ks0_index2 = Ks0_temp[5]; 

			for(int j=0;j<ProtonM_vector->size();j++){
				vector<double> ProtonM_temp = ProtonM_vector -> at(j);
				int ProtonM_index = ProtonM_temp[4]; 
			
				if(ProtonM_index==Ks0_index1||ProtonM_index==Ks0_index2){
					continue;
				}
				TLorentzVector P4_total(Ks0_temp[0]+ProtonM_temp[0],
										Ks0_temp[1]+ProtonM_temp[1],
										Ks0_temp[2]+ProtonM_temp[2],
										Ks0_temp[3]+ProtonM_temp[3]);
			
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();

				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index[0]=Ks0_index1;
					index[1]=Ks0_index2;
					index[2]=ProtonM_index;
		
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			}//pion
		}//lbd
		
		if(pass==true){
			STindex.push_back(index[0]);	
			STindex.push_back(index[1]);	
			STindex.push_back(index[2]);
			XiInfo = Xi_vector;
			output -> Fill();
		}

		}

	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}










