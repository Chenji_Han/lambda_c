/*************************************************************************
    > File Name: PKs2Pi.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 08:04:22 PM CST
 ************************************************************************/
#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void PKs2Pi(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}

	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* AllTracks;
	vector<vector<double> >* Ks0_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;
	vector<vector<double> >* ProtonP_vector;
	vector<vector<double> >* ProtonM_vector;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;

	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);
	inputTree -> SetBranchAddress("AllTracks",&AllTracks);
	inputTree -> SetBranchAddress("Ks0",&Ks0_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);
	inputTree -> SetBranchAddress("ProtonP",&ProtonP_vector);
	inputTree -> SetBranchAddress("ProtonM",&ProtonM_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_PKs2Pi_DT_" + flag_s+veto_s + ".root";
	
	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);

	bool pass = false;
	int passNum = 0;

	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
	//	if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;

		pass = false; dE =1.0; Mbc =0.0;
		int index[5];
		if(flag==1||flag==3){
		for(int i=0;i<Ks0_vector->size();i++){
			vector<double> Ks0_temp = Ks0_vector -> at(i);
			int Ks0_index1 = Ks0_temp[4]; 
			int Ks0_index2 = Ks0_temp[5]; 

			for(int j=0;j<ProtonP_vector->size();j++){
				vector<double> ProtonP_temp = ProtonP_vector -> at(j);
				int ProtonP_index = ProtonP_temp[4]; 
				
				if(ProtonP_index==Ks0_index1||ProtonP_index==Ks0_index2)	continue;
				
				if(veto==1){
					if(bkg_veto(ProtonP_index,mp,Ks0_index2,mpi,AllTracks,1.11,1.12)){
						continue;
					}
				}

				for(int k=0;k<PionP_vector->size();k++){
					vector<double> PionP_temp = PionP_vector -> at(k);
					int PionP_index = PionP_temp[4];
					if(PionP_index==Ks0_index1) continue;
					if(PionP_index==Ks0_index2) continue;
					if(PionP_index==ProtonP_index) continue;


					for(int m=0;m<PionM_vector->size();m++){
						vector<double> PionM_temp = PionM_vector -> at(m);
						int PionM_index = PionM_temp[4];
						if(PionM_index==Ks0_index1) continue;
						if(PionM_index==Ks0_index2) continue;
						if(PionM_index==ProtonP_index) continue;
						if(PionM_index==PionP_index) continue;

						if(veto==1){
							if(bkg_veto(ProtonP_index,mp,PionM_index,mpi,AllTracks,1.11,1.12)){
								continue;
							}
						}
							
						TLorentzVector P4_total(ProtonP_temp[0]+Ks0_temp[0]+PionP_temp[0]+PionM_temp[0],
												ProtonP_temp[1]+Ks0_temp[1]+PionP_temp[1]+PionM_temp[1],
												ProtonP_temp[2]+Ks0_temp[2]+PionP_temp[2]+PionM_temp[2],
												ProtonP_temp[3]+Ks0_temp[3]+PionP_temp[3]+PionM_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=Ks0_index1;
							index[1]=Ks0_index2;
							index[2]=PionP_index;
							index[3]=PionM_index;
							index[4]=ProtonP_index;
							dE = dE_temp;
							pass = true;
							Mbc = Get_Mbc(P4_total);
						}
					}//pionm
				}//pionp
			}//pionp
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			STindex.push_back(index[3]);
			STindex.push_back(index[4]);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}

		}

		//c.c.
		pass = false; dE =1.0; Mbc =0.0;
	
		if(flag==2||flag==3){
		for(int i=0;i<Ks0_vector->size();i++){
			vector<double> Ks0_temp = Ks0_vector -> at(i);
			int Ks0_index1 = Ks0_temp[4]; 
			int Ks0_index2 = Ks0_temp[5]; 

			for(int j=0;j<ProtonM_vector->size();j++){
				vector<double> ProtonM_temp = ProtonM_vector -> at(j);
				int ProtonM_index = ProtonM_temp[4]; 
				
				if(ProtonM_index==Ks0_index1||ProtonM_index==Ks0_index2)	continue;
				
				if(veto==1){
					if(bkg_veto(ProtonM_index,mp,Ks0_index1,mpi,AllTracks,1.11,1.12)){
						continue;
					}
				}

				for(int k=0;k<PionP_vector->size();k++){
					vector<double> PionP_temp = PionP_vector -> at(k);
					int PionP_index = PionP_temp[4];
					if(PionP_index==Ks0_index1) continue;
					if(PionP_index==Ks0_index2) continue;
					if(PionP_index==ProtonM_index) continue;

					if(veto==1){
						if(bkg_veto(ProtonM_index,mp,PionP_index,mpi,AllTracks,1.11,1.12)){
							continue;
						}
					}
							
					for(int m=0;m<PionM_vector->size();m++){
						vector<double> PionM_temp = PionM_vector -> at(m);
						int PionM_index = PionM_temp[4];
						if(PionM_index==Ks0_index1) continue;
						if(PionM_index==Ks0_index2) continue;
						if(PionM_index==ProtonM_index) continue;
						if(PionM_index==PionP_index) continue;

						TLorentzVector P4_total(ProtonM_temp[0]+Ks0_temp[0]+PionP_temp[0]+PionM_temp[0],
												ProtonM_temp[1]+Ks0_temp[1]+PionP_temp[1]+PionM_temp[1],
												ProtonM_temp[2]+Ks0_temp[2]+PionP_temp[2]+PionM_temp[2],
												ProtonM_temp[3]+Ks0_temp[3]+PionP_temp[3]+PionM_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=Ks0_index1;
							index[1]=Ks0_index2;
							index[2]=PionP_index;
							index[3]=PionM_index;
							index[4]=ProtonM_index;
							dE = dE_temp;
							pass = true;
							Mbc = Get_Mbc(P4_total);
						}
					}//pionm
				}//pionp
			}//pionp
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			STindex.push_back(index[3]);
			STindex.push_back(index[4]);
			XiInfo = Xi_vector;
			outputTree -> Fill();
		}

		}
	
	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}








