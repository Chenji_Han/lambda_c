/*************************************************************************
    > File Name: LbdPi.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 02:35:54 AM CST
 ************************************************************************/
#include<iostream>
#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void LbdPiPi0(){

	int flag=-1;
	int veto=-1;

	vector<string> inputName;
	
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
		
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
		if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}

	}

	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}
	
	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* Lbd_vector;
	vector<vector<double> >* ALbd_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;
	vector<vector<double> >* Pi0_vector;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;


	inputTree -> SetBranchAddress("ALbd",&ALbd_vector);
	inputTree -> SetBranchAddress("Lbd",&Lbd_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);
	inputTree -> SetBranchAddress("Pi0",&Pi0_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);
	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	
	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_LbdPiPi0_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);

	bool pass = false;
	int passNum = 0;

	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
		//if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;

		pass = false; dE =1.0; Mbc =0.0;

		int index[3];
		if(flag==1||flag==3){
		for(int i=0;i<Lbd_vector->size();i++){
			vector<double> Lbd_temp = Lbd_vector -> at(i);
			int Lbd_index1 = Lbd_temp[4]; 
			int Lbd_index2 = Lbd_temp[5]; 

			for(int j=0;j<PionP_vector->size();j++){
				vector<double> PionP_temp = PionP_vector -> at(j);
				int PionP_index = PionP_temp[4]; 
				
				if(PionP_index==Lbd_index1||PionP_index==Lbd_index2){
					continue;
				}

				for(int k=0;k<Pi0_vector->size();k++){
					vector<double> Pi0_temp = Pi0_vector -> at(k);
				
					TLorentzVector P4_total(Lbd_temp[0]+PionP_temp[0]+Pi0_temp[0],
											Lbd_temp[1]+PionP_temp[1]+Pi0_temp[1],
											Lbd_temp[2]+PionP_temp[2]+Pi0_temp[2],
											Lbd_temp[3]+PionP_temp[3]+Pi0_temp[3]);
					P4_total.Boost(-0.011,0,0);
	
					double dE_temp = 2.3-P4_total.E();
					if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
						index[0] = PionP_index;
						index[1] = Lbd_index1;
						index[2] = Lbd_index1;
						dE = dE_temp;
						pass = true;
						Mbc = Get_Mbc(P4_total);
					}

				}//pi0
			}//pion
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}

		}
		
		pass = false; dE =1.0; Mbc =0.0;

		if(flag==2||flag==3){
		for(int i=0;i<ALbd_vector->size();i++){
			vector<double> ALbd_temp = ALbd_vector -> at(i);
			int ALbd_index1 = ALbd_temp[4]; 
			int ALbd_index2 = ALbd_temp[5]; 

			for(int j=0;j<PionM_vector->size();j++){
				vector<double> PionM_temp = PionM_vector -> at(j);
				int PionM_index = PionM_temp[4]; 
				
				if(PionM_index==ALbd_index1||PionM_index==ALbd_index2){
					continue;
				}

				for(int k=0;k<Pi0_vector->size();k++){
					vector<double> Pi0_temp = Pi0_vector -> at(k);
				
					TLorentzVector P4_total(ALbd_temp[0]+PionM_temp[0]+Pi0_temp[0],
											ALbd_temp[1]+PionM_temp[1]+Pi0_temp[1],
											ALbd_temp[2]+PionM_temp[2]+Pi0_temp[2],
											ALbd_temp[3]+PionM_temp[3]+Pi0_temp[3]);
					P4_total.Boost(-0.011,0,0);
					double dE_temp = 2.3-P4_total.E();
					if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
						index[0] = PionM_index;
						index[1] = ALbd_index1;
						index[2] = ALbd_index2;
						dE = dE_temp;
						pass = true;
						Mbc = Get_Mbc(P4_total);
					}

				}//pi0
			}//pion
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			XiInfo = Xi_vector;
			outputTree -> Fill();
		}

		}

	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}










