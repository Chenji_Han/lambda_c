/*************************************************************************
    > File Name: Lbd3Pi.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 08:04:22 PM CST
 ************************************************************************/
#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void Lbd3Pi(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}

	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;
	vector<vector<double> >* AllTracks;
	vector<vector<double> >* Lbd_vector;
	vector<vector<double> >* ALbd_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;

	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);
	inputTree -> SetBranchAddress("AllTracks",&AllTracks);
	inputTree -> SetBranchAddress("Lbd",&Lbd_vector);
	inputTree -> SetBranchAddress("ALbd",&ALbd_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_Lbd3Pi_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;
	
	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);
	
	bool pass = false;
	int passNum = 0;
	
	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
//		if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;
		pass = false; dE =1.0; Mbc =0.0;
		
		int index[5];
		if((flag==1||flag==3)&&PionP_vector->size()>=2){
		for(int i=0;i<Lbd_vector->size();i++){
			vector<double> Lbd_temp = Lbd_vector -> at(i);
			int Lbd_index1 = Lbd_temp[4]; 
			int Lbd_index2 = Lbd_temp[5]; 

			for(int j=0;j<PionP_vector->size()-1;j++){
				vector<double> PionP1_temp = PionP_vector -> at(j);
				int PionP1_index = PionP1_temp[4]; 
				
				if(PionP1_index==Lbd_index1||PionP1_index==Lbd_index2)	continue;
				if(veto==1){
					if(bkg_veto(Lbd_index2,mpi,PionP1_index,mpi,AllTracks,0.48,0.52)){
						continue;
					}
				}

				for(int k=j+1;k<PionP_vector->size();k++){
					vector<double> PionP2_temp = PionP_vector -> at(k);
					int PionP2_index = PionP2_temp[4];
					if(veto==1){
						if(bkg_veto(Lbd_index2,mpi,PionP2_index,mpi,AllTracks,0.48,0.52)){
							continue;
						}
					}
					
					for(int m=0;m<PionM_vector->size();m++){
						vector<double> PionM_temp = PionM_vector -> at(m);
						int PionM_index = PionM_temp[4];
						if(veto==1){
							if(bkg_veto(PionM_index,mpi,PionP2_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
						if(veto==1){
							if(bkg_veto(PionM_index,mpi,PionP1_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
						
						TLorentzVector P4_total(Lbd_temp[0]+PionP1_temp[0]+PionP2_temp[0]+PionM_temp[0],
												Lbd_temp[1]+PionP1_temp[1]+PionP2_temp[1]+PionM_temp[1],
												Lbd_temp[2]+PionP1_temp[2]+PionP2_temp[2]+PionM_temp[2],
												Lbd_temp[3]+PionP1_temp[3]+PionP2_temp[3]+PionM_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0] = Lbd_index1;
							index[1] = Lbd_index1;
							index[2] = PionP1_index;
							index[3] = PionP2_index;
							index[4] = PionM_index;
							dE = dE_temp;
							pass = true;
							Mbc = Get_Mbc(P4_total);
						}
					}//pionm
				}//pionp
			}//pionp
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			STindex.push_back(index[3]);
			STindex.push_back(index[4]);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}

		}

		//c.c.
		pass = false; dE =1.0; Mbc =0.0;
		
		if((flag==2||flag==3)&&PionM_vector->size()>=2){
		for(int i=0;i<ALbd_vector->size();i++){
			vector<double> ALbd_temp = ALbd_vector -> at(i);
			int ALbd_index1 = ALbd_temp[4]; 
			int ALbd_index2 = ALbd_temp[5]; 

			for(int j=0;j<PionM_vector->size()-1;j++){
				vector<double> PionM1_temp = PionM_vector -> at(j);
				int PionM1_index = PionM1_temp[4]; 
				
				if(PionM1_index==ALbd_index1||PionM1_index==ALbd_index2)	continue;
				if(veto==1){
					if(bkg_veto(ALbd_index2,mpi,PionM1_index,mpi,AllTracks,0.48,0.52)){
						continue;
					}
				}

				for(int k=j+1;k<PionM_vector->size();k++){
					vector<double> PionM2_temp = PionM_vector -> at(k);
					int PionM2_index = PionM2_temp[4];
				
					if(veto==1){
						if(bkg_veto(ALbd_index2,mpi,PionM2_index,mpi,AllTracks,0.48,0.52)){
							continue;
						}
					}
					
					for(int m=0;m<PionP_vector->size();m++){
						vector<double> PionP_temp = PionP_vector -> at(m);
						int PionP_index = PionP_temp[4];
						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM2_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM1_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
								
						TLorentzVector P4_total(ALbd_temp[0]+PionM1_temp[0]+PionM2_temp[0]+PionP_temp[0],
												ALbd_temp[1]+PionM1_temp[1]+PionM2_temp[1]+PionP_temp[1],
												ALbd_temp[2]+PionM1_temp[2]+PionM2_temp[2]+PionP_temp[2],
												ALbd_temp[3]+PionM1_temp[3]+PionM2_temp[3]+PionP_temp[3]);
						P4_total.Boost(-0.011,0,0);
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0] = ALbd_index1;
							index[1] = ALbd_index1;
							index[2] = PionM1_index;
							index[3] = PionM2_index;
							index[4] = PionP_index;
		
							dE = dE_temp;
							pass = true;
							Mbc = Get_Mbc(P4_total);
						}
					}//pionm
				}//pionp
			}//pionp
		}//lbd
	
		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			STindex.push_back(index[3]);
			STindex.push_back(index[4]);
			XiInfo = Xi_vector;
			outputTree -> Fill();
		}

		}
	
	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}








