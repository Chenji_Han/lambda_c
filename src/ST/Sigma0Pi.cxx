/*************************************************************************
    > File Name: Sigma0Pi.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 02:35:54 AM CST
 ************************************************************************/

#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void Sigma0Pi(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}

	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* Sigma0_vector;
	vector<vector<double> >* ASigma0_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;

	inputTree -> SetBranchAddress("ASigma0",&ASigma0_vector);
	inputTree -> SetBranchAddress("Sigma0",&Sigma0_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);
	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_Sigma0Pi_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);

	bool pass = false;
	int passNum = 0;
	
	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
//		if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;

		pass = false; dE =1.0; Mbc =0.0;
		int index[3];
		if(flag==1||flag==3){
		for(int i=0;i<Sigma0_vector->size();i++){
			vector<double> Sigma0_temp = Sigma0_vector -> at(i);
			int Sigma0_index1 = Sigma0_temp[4]; 
			int Sigma0_index2 = Sigma0_temp[5]; 
			int Sigma0_index3 = Sigma0_temp[6]; 

			for(int j=0;j<PionP_vector->size();j++){
				vector<double> PionP_temp = PionP_vector -> at(j);
				int PionP_index = PionP_temp[4]; 
			
				if(PionP_index==Sigma0_index1||PionP_index==Sigma0_index2){
					continue;
				}

				TLorentzVector P4_total(Sigma0_temp[0]+PionP_temp[0],
										Sigma0_temp[1]+PionP_temp[1],
										Sigma0_temp[2]+PionP_temp[2],
										Sigma0_temp[3]+PionP_temp[3]);
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();

				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index[0]=Sigma0_index1;
					index[1]=Sigma0_index2;
					index[2]=PionP_index;
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			
			}//pion
		}//lbd

		if(pass==true){
			STindex.push_back(index[0]);
			STindex.push_back(index[1]);
			STindex.push_back(index[2]);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}
		
		}

		pass = false; dE =1.0; Mbc =0.0;
		if(flag==2||flag==3){
		for(int i=0;i<ASigma0_vector->size();i++){
			vector<double> ASigma0_temp = ASigma0_vector -> at(i);
			int ASigma0_index1 = ASigma0_temp[4]; 
			int ASigma0_index2 = ASigma0_temp[5]; 

			for(int j=0;j<PionM_vector->size();j++){
				vector<double> PionM_temp = PionM_vector -> at(j);
				int PionM_index = PionM_temp[4]; 
			
				if(PionM_index==ASigma0_index1||PionM_index==ASigma0_index2){
					continue;
				}
				TLorentzVector P4_total(ASigma0_temp[0]+PionM_temp[0],
										ASigma0_temp[1]+PionM_temp[1],
										ASigma0_temp[2]+PionM_temp[2],
										ASigma0_temp[3]+PionM_temp[3]);
			
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();

				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index[0]=ASigma0_index1;
					index[1]=ASigma0_index2;
					index[2]=PionM_index;
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			}//pion
		}//lbd
		
		if(pass==true){
			STindex.push_back(index[0]);	
			STindex.push_back(index[1]);	
			STindex.push_back(index[2]);
			XiInfo = Xi_vector;
			outputTree -> Fill();
		}

		}

	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}










