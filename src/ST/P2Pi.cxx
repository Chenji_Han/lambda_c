/*************************************************************************
    > File Name: P2Pi.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 08:04:22 PM CST
 ************************************************************************/
#include<string>
#include<vector>
#include"TLorentzVector.h"
#include"../functions.cxx"

void P2Pi(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}


	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* ProtonP_vector;
	vector<vector<double> >* AllTracks;
	vector<vector<double> >* ProtonM_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;

	inputTree -> SetBranchAddress("AllTracks",&AllTracks);
	inputTree -> SetBranchAddress("ProtonP",&ProtonP_vector);
	inputTree -> SetBranchAddress("ProtonM",&ProtonM_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);
	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_P2Pi_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);


	bool pass = false;
	int passNum = 0 ;
	int evtNum = inputTree -> GetEntries();
	for(int ievt=0;ievt<evtNum;ievt++){
	
		inputTree -> GetEntry(ievt);
		//if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;
	
		pass = false; dE =10; Mbc =0.0;
		int index[3];
		if(flag==1||flag==3){

			for( int i=0;i<ProtonP_vector->size();i++){
				vector<double> ProtonP_temp = ProtonP_vector -> at(i);
				int ProtonP_index = ProtonP_temp[4]; 
				
				for( int j=0;j<PionM_vector->size();j++){
					vector<double> PionM_temp = PionM_vector -> at(j);
					int PionM_index = PionM_temp[4];

					if(ProtonP_index==PionM_index) continue;
					
					if(veto==1){
						if(bkg_veto(ProtonP_index,mp,PionM_index,mpi,AllTracks,1.11,1.12)){
							continue;
						}
					}

					for( int k=0;k<PionP_vector->size();k++){
						vector<double> PionP_temp = PionP_vector -> at(k);
						int PionP_index = PionP_temp[4];
						
						if(PionP_index==ProtonP_index||PionP_index==PionM_index) continue;
						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
			
						TLorentzVector P4_total(ProtonP_temp[0]+PionM_temp[0]+PionP_temp[0],
												ProtonP_temp[1]+PionM_temp[1]+PionP_temp[1],
												ProtonP_temp[2]+PionM_temp[2]+PionP_temp[2],
												ProtonP_temp[3]+PionM_temp[3]+PionP_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=ProtonP_index;
							index[1]=PionP_index;
							index[2]=PionM_index;
							dE = dE_temp;
							Mbc = Get_Mbc(P4_total);
							pass = true;
						}
					}//pionm
				}//pionp
			}//pionp
	
			if(pass==true){
				STindex.push_back(index[0]);
				STindex.push_back(index[1]);
				STindex.push_back(index[2]);
				XiInfo = XiBar_vector;
				outputTree -> Fill();
			}
		
		}

		//c.c.
		pass = 0; dE =10; Mbc =0.0;
	
		if(flag==2||flag==3){

			for( int i=0;i<ProtonM_vector->size();i++){
				vector<double> ProtonM_temp = ProtonM_vector -> at(i);
				int ProtonM_index = ProtonM_temp[4]; 
				
				for( int j=0;j<PionM_vector->size();j++){
					vector<double> PionM_temp = PionM_vector -> at(j);
					int PionM_index = PionM_temp[4];

					if(ProtonM_index==PionM_index) continue;
						
					for( int k=0;k<PionP_vector->size();k++){
						vector<double> PionP_temp = PionP_vector -> at(k);
						int PionP_index = PionP_temp[4];
						
						if(PionP_index==ProtonM_index||PionP_index==PionM_index) continue;

						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
						if(veto==1){
							if(bkg_veto(ProtonM_index,mp,PionP_index,mpi,AllTracks,1.11,1.12)){
								continue;
							}
						}

						TLorentzVector P4_total(ProtonM_temp[0]+PionM_temp[0]+PionP_temp[0],
												ProtonM_temp[1]+PionM_temp[1]+PionP_temp[1],
												ProtonM_temp[2]+PionM_temp[2]+PionP_temp[2],
												ProtonM_temp[3]+PionM_temp[3]+PionP_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=ProtonM_index;
							index[1]=PionP_index;
							index[2]=PionM_index;
							dE = dE_temp;
							Mbc = Get_Mbc(P4_total);
							pass = true;
						}
					}//pionm
				}//pionp
			}//pionp
	
			if(pass==true){
				STindex.push_back(index[0]);	
				STindex.push_back(index[1]);	
				STindex.push_back(index[2]);
				XiInfo = Xi_vector;
				outputTree -> Fill();
			}
		
		}


	
	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;


//	cout<<"num of ProtonP = "<<ProtonP_Num<<endl;
//	cout<<"num of KaonM   = "<<KaonM_Num<<endl;
//	cout<<"num of PionP   = "<<PionP_Num<<endl;
//
//	cout<<"num of ProtonM = "<<ProtonM_Num<<endl;
//	cout<<"num of KaonP   = "<<KaonP_Num<<endl;
//	cout<<"num of PionM   = "<<PionM_Num<<endl;
}



