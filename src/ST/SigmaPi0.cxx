/*************************************************************************
    > File Name: SigmaPi0.cpp
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 02:35:54 AM CST
 ************************************************************************/

#include<string>
#include<vector>
#include"TLorentzVector.h"

#include"../functions.cxx"

void SigmaPi0(){

	int flag=-1, veto=-1;

	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}


	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}

	vector<vector<double> >* SigmaP_vector;
	vector<vector<double> >* SigmaM_vector;
	vector<vector<double> >* Pi0_vector;
	vector<vector<double> >* Gamma_vector;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;

	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);
	inputTree -> SetBranchAddress("SigmaP",&SigmaP_vector);
	inputTree -> SetBranchAddress("SigmaM",&SigmaM_vector);
	inputTree -> SetBranchAddress("Pi0",&Pi0_vector);
	inputTree -> SetBranchAddress("Gamma",&Gamma_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_SigmaPi0_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);

	bool pass = false;
	int passNum = 0;
	
	for(int ievt=0;ievt<inputTree->GetEntries();ievt++){
		inputTree -> GetEntry(ievt);
//		if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;

		pass = false; dE =1.0; Mbc =0.0;
		int index;
		if(flag==1||flag==3){
		for(int i=0;i<SigmaP_vector->size();i++){
			vector<double> SigmaP_temp = SigmaP_vector -> at(i);
			int SigmaP_Pindex1 = SigmaP_temp[4]; 
			int SigmaP_Gamindex1 = SigmaP_temp[5]; 
			int SigmaP_Gamindex2 = SigmaP_temp[6]; 

			for(int j=0;j<Pi0_vector->size();j++){
				vector<double> Pi0_temp = Pi0_vector -> at(j);
				int Pi0_index1 = Pi0_temp[4]; 
				int Pi0_index2 = Pi0_temp[5]; 
			
				if(Pi0_index1==SigmaP_Gamindex1||Pi0_index1==SigmaP_Gamindex2){
					continue;
				}
				if(Pi0_index2==SigmaP_Gamindex1||Pi0_index2==SigmaP_Gamindex2){
					continue;
				}
				if(veto==1){
					if(bkg_veto(SigmaP_Gamindex1,SigmaP_Gamindex2,Gamma_vector,Pi0_temp,0.48,0.52)){
						continue;
					}
				}

				TLorentzVector P4_total(SigmaP_temp[0]+Pi0_temp[0],
										SigmaP_temp[1]+Pi0_temp[1],
										SigmaP_temp[2]+Pi0_temp[2],
										SigmaP_temp[3]+Pi0_temp[3]);
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();
				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index = SigmaP_Pindex1;
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			
			}//pion
		}//lbd
		
		if(pass==true){
			STindex.push_back(index);
			XiInfo = XiBar_vector;
			outputTree -> Fill();
		}

		}

		pass = false; dE =1.0; Mbc =0.0;

		if(flag==2||flag==3){
		for(int i=0;i<SigmaM_vector->size();i++){
			vector<double> SigmaM_temp = SigmaM_vector -> at(i);
			int SigmaM_Pindex1 = SigmaM_temp[4]; 
			int SigmaM_Gamindex1 = SigmaM_temp[5]; 
			int SigmaM_Gamindex2 = SigmaM_temp[6]; 

			for(int j=0;j<Pi0_vector->size();j++){
				vector<double> Pi0_temp = Pi0_vector -> at(j);
				int Pi0_index1 = Pi0_temp[4]; 
				int Pi0_index2 = Pi0_temp[5]; 
			
				if(Pi0_index1==SigmaM_Gamindex1||Pi0_index1==SigmaM_Gamindex2){
					continue;
				}
				if(Pi0_index2==SigmaM_Gamindex1||Pi0_index2==SigmaM_Gamindex2){
					continue;
				}
				if(veto==1){
					if(bkg_veto(SigmaM_Gamindex1,SigmaM_Gamindex2,Gamma_vector,Pi0_temp,0.48,0.52)){
						continue;
					}
				}

				TLorentzVector P4_total(SigmaM_temp[0]+Pi0_temp[0],
										SigmaM_temp[1]+Pi0_temp[1],
										SigmaM_temp[2]+Pi0_temp[2],
										SigmaM_temp[3]+Pi0_temp[3]);
				P4_total.Boost(-0.011,0,0);

				double dE_temp = 2.3-P4_total.E();
				if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
					index = SigmaM_Pindex1;
					dE = dE_temp;
					pass = true;
					Mbc = Get_Mbc(P4_total);
				}
			
			}//pion
		}//lbd
		
		if(pass==true){
			STindex.push_back(index);
			XiInfo = Xi_vector;
			outputTree -> Fill();
		}

		}

	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;

}










