/*************************************************************************
    > File Name: Sigma2Pi.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 08:04:22 PM CST
 ************************************************************************/
#include<string>
#include<vector>
#include"TLorentzVector.h"
#include"../functions.cxx"

void Sigma2Pi(){

	int flag=-1, veto=-1;
	vector<string> inputName;
	string line;
    std::ifstream inputTxt("inputs.txt");
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="reco_flag"){
			flag = (int) string2double(b);
		}else if(a=="reco_veto"){
			veto = (int) string2double(b);
		}else if(a=="reco_inputName"){
			inputName.push_back(b);
		}
	}
	if(flag==-1||veto==-1){
		cout<<"invalid inputs!"<<endl;
		return;
	}


	TChain* inputTree = new TChain("output");
	for(int i=0;i<inputName.size();i++){
		cout<<"adding "<<inputName[i]<<endl;
		inputTree -> Add(inputName[i].c_str());
	}


	vector<vector<double> >* SigmaM_vector;
	vector<vector<double> >* SigmaP_vector;
	vector<vector<double> >* PionP_vector;
	vector<vector<double> >* PionM_vector;
	vector<vector<double> >* AllTracks;
	vector<vector<double> >* Xi_vector;
	vector<vector<double> >* XiBar_vector;

	inputTree -> SetBranchAddress("Xi",&Xi_vector);
	inputTree -> SetBranchAddress("XiBar",&XiBar_vector);
	inputTree -> SetBranchAddress("AllTracks",&AllTracks);
	inputTree -> SetBranchAddress("SigmaM",&SigmaM_vector);
	inputTree -> SetBranchAddress("SigmaP",&SigmaP_vector);
	inputTree -> SetBranchAddress("PionP",&PionP_vector);
	inputTree -> SetBranchAddress("PionM",&PionM_vector);

	string flag_s,veto_s;
	stringstream ss; ss<<flag; flag_s = ss.str();
	stringstream sss; sss<<veto; veto_s = sss.str();	
	string outputName;
	size_t suffix = inputName[0].find_last_of(".");
	string prefix = inputName[0].substr(0,suffix);
	outputName = prefix + "_Sigma2Pi_DT_" + flag_s+veto_s + ".root";

	TFile* outputFile = new TFile(outputName.c_str(),"recreate");
	TTree* outputTree = new TTree("output","");

	double dE=1.0, Mbc=0.0;
	vector<vector<double> > XiInfo;
	vector<int> STindex;

	outputTree -> Branch("dE",&dE);
	outputTree -> Branch("Mbc",&Mbc);
	outputTree -> Branch("Xi",&XiInfo);
	outputTree -> Branch("STindex",&STindex);


	bool pass = false;
	int passNum = 0 ;
	int evtNum = inputTree -> GetEntries();
	for(int ievt=0;ievt<evtNum;ievt++){
	
		inputTree -> GetEntry(ievt);
//		if(ievt%100000==0) cout<<"processing..."<<ievt<<endl;
	
		pass = false; dE =10; Mbc =0.0;
		int index[3];	
		if(flag==1||flag==3){
			for( int i=0;i<SigmaP_vector->size();i++){
				vector<double> SigmaP_temp = SigmaP_vector -> at(i);
				int SigmaP_index = SigmaP_temp[4]; 
				
				for( int j=0;j<PionM_vector->size();j++){
					vector<double> PionM_temp = PionM_vector -> at(j);
					int PionM_index = PionM_temp[4];

					if(SigmaP_index==PionM_index) continue;
					
					if(veto==1){
						if(bkg_veto(SigmaP_index,mp,PionM_index,mpi,AllTracks,1.11,1.12)){
							continue;
						}
					}

					for( int k=0;k<PionP_vector->size();k++){
						vector<double> PionP_temp = PionP_vector -> at(k);
						int PionP_index = PionP_temp[4];
						
						if(PionP_index==SigmaP_index||PionP_index==PionM_index) continue;
						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
			
						TLorentzVector P4_total(SigmaP_temp[0]+PionM_temp[0]+PionP_temp[0],
												SigmaP_temp[1]+PionM_temp[1]+PionP_temp[1],
												SigmaP_temp[2]+PionM_temp[2]+PionP_temp[2],
												SigmaP_temp[3]+PionM_temp[3]+PionP_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=SigmaP_index;
							index[1]=PionP_index;
							index[2]=PionM_index;
							dE = dE_temp;
							Mbc = Get_Mbc(P4_total);
							pass = true;
						}
					}//pionm
				}//pionp
			}//pionp
	
			if(pass==true){
				STindex.push_back(index[0]);
				STindex.push_back(index[1]);
				STindex.push_back(index[2]);
				XiInfo = XiBar_vector;
				outputTree -> Fill();
			}
		
		}

		//c.c.
		pass = 0; dE =10; Mbc =0.0;
	
		if(flag==2||flag==3){

			for( int i=0;i<SigmaM_vector->size();i++){
				vector<double> SigmaM_temp = SigmaM_vector -> at(i);
				int SigmaM_index = SigmaM_temp[4]; 
				
				for( int j=0;j<PionM_vector->size();j++){
					vector<double> PionM_temp = PionM_vector -> at(j);
					int PionM_index = PionM_temp[4];

					if(SigmaM_index==PionM_index) continue;
					
					for( int k=0;k<PionP_vector->size();k++){
						vector<double> PionP_temp = PionP_vector -> at(k);
						int PionP_index = PionP_temp[4];
						
						if(PionP_index==SigmaM_index||PionP_index==PionM_index) continue;
						if(veto==1){
							if(bkg_veto(PionP_index,mpi,PionM_index,mpi,AllTracks,0.48,0.52)){
								continue;
							}
						}
						if(veto==1){
							if(bkg_veto(SigmaM_index,mp,PionP_index,mpi,AllTracks,1.11,1.12)){
								continue;
							}
						}
		
						TLorentzVector P4_total(SigmaM_temp[0]+PionM_temp[0]+PionP_temp[0],
												SigmaM_temp[1]+PionM_temp[1]+PionP_temp[1],
												SigmaM_temp[2]+PionM_temp[2]+PionP_temp[2],
												SigmaM_temp[3]+PionM_temp[3]+PionP_temp[3]);
						P4_total.Boost(-0.011,0,0);
	
						double dE_temp = 2.3-P4_total.E();
						if(TMath::Abs(dE_temp)<TMath::Abs(dE)){
							index[0]=SigmaM_index;
							index[1]=PionP_index;
							index[2]=PionM_index;
							dE = dE_temp;
							Mbc = Get_Mbc(P4_total);
							pass = true;
						}
					}//pionm
				}//pionp
			}//pionp
	
			if(pass==true){
				STindex.push_back(index[0]);
				STindex.push_back(index[1]);
				STindex.push_back(index[2]);
				XiInfo = Xi_vector;
				outputTree -> Fill();
			}
		
		}

	}//trees

	outputFile -> cd();
	outputTree -> Write();
	outputFile -> Close();

	cout<<"the number of reconstructed events = "<<passNum<<endl;


}



