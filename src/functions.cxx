/*************************************************************************
    > File Name: functions.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Thu 27 Jun 2019 07:53:37 PM CST

 
 
 ************************************************************************/

#include"functions.h"

bool IsOverlap(vector<double> XiInfo, vector<int>* STindex){
	
	for(int i=0;i<STindex->size();i++){
		int tag = STindex -> at(i);

		if(tag == (int) XiInfo[17]) return true;
		if(tag == (int) XiInfo[28]) return true;
		if(tag == (int) XiInfo[39]) return true;
	}

	return false;
}


double string2double(string input){
	
	int index = 0;
	double pre = 0.0;
	double aft = 0.0;
	bool comma = false;
	double num = 1.0;
	double charge;
	if(input[0]=='-') 
		charge = -1.0;
	else 
		charge = 1.0;

	while(input[index]!='\0'){
		if(input[index]=='.'){
			comma = true;
		}
		if('0'<=input[index]&&input[index]<='9'){
			double temp = input[index] - '0';
			if(comma==false){
				pre = pre*10.0 + temp;
			}else{
				num *= 10.0;
				aft += temp/num;
			}
		}
		index++;
	}

	return charge * (pre+aft) ;

}


double Get_Mbc(TLorentzVector P4_total){

	double p_rho = P4_total.Rho();
	double Mbc2 = 2.3*2.3 - p_rho*p_rho;	

	return (Mbc2>0)? sqrt(Mbc2) : 0 ;
}

bool bkg_veto(int track1,int track2,vector<vector<double> >* Gamma, vector<double> P4_Pi0,double range1,double range2){

	int get1=0,get2=0;
	vector<double> P4_1,P4_2;
	for(int i=0;i<Gamma->size();i++){
		vector<double> Track = Gamma -> at(i);
		if(Track[4]==(double)track1){
			P4_1 = Gamma -> at(i);
			get1 = 1;
		}else if(Track[4]==(double)track2){
			P4_2 =  Gamma -> at(i);
			get2 = 1;
		}
		if(get1==1&&get2==1){
			break;
		}
	}

	TLorentzVector P4(P4_1[0]+P4_2[0]+P4_Pi0[0],
					  P4_1[1]+P4_2[1]+P4_Pi0[1],
					  P4_1[2]+P4_2[2]+P4_Pi0[2],
					  P4_1[3]+P4_2[2]+P4_Pi0[3]);
	double M = P4.M();
	if(range1<M&&M<range2){
		return true;
	}else{
		return false;
	}

}


bool bkg_veto(int track1,double m1, int track2,double m2,vector<vector<double> >* AllTracks, vector<double> P4_Pi0,double range1,double range2){

	int get1=0,get2=0;
	vector<double> P4_1,P4_2;
	for(int i=0;i<AllTracks->size();i++){
		vector<double> Track = AllTracks -> at(i);
		if(Track[3]==(double)track1){
			P4_1 = AllTracks -> at(i);
			get1 = 1;
		}else if(Track[3]==(double)track2){
			P4_2 =  AllTracks -> at(i);
			get2 = 1;
		}

		if(get1==1&&get2==1){
			break;
		}
	}
	double E1 = sqrt(P4_1[0]*P4_1[0]+P4_1[1]*P4_1[1]+P4_1[2]*P4_1[2]+m1*m1);
	double E2 = sqrt(P4_2[0]*P4_2[0]+P4_2[1]*P4_2[1]+P4_2[2]*P4_2[2]+m2*m2);

	TLorentzVector P4(P4_1[0]+P4_2[0]+P4_Pi0[0],
					  P4_1[1]+P4_2[1]+P4_Pi0[1],
					  P4_1[2]+P4_2[2]+P4_Pi0[2],
					  E1+E2+P4_Pi0[3]);
	double M = P4.M();
	if(range1<M&&M<range2){
		return true;
	}else{
		return false;
	}

}

bool bkg_veto(int track1,double m1,vector<vector<double> >* AllTracks, vector<double> P4_Pi0,double range1,double range2){

	int get1=0;
	vector<double> P4_1;
	for(int i=0;i<AllTracks->size();i++){
		vector<double> Track = AllTracks -> at(i);
		if(Track[3]==(double)track1){
			P4_1 = AllTracks -> at(i);
			get1 = 1;
		}
		if(get1==1){
			break;
		}
	}
	double E1 = sqrt(P4_1[0]*P4_1[0]+P4_1[1]*P4_1[1]+P4_1[2]*P4_1[2]+m1*m1);

	TLorentzVector P4(P4_1[0]+P4_Pi0[0],
					  P4_1[1]+P4_Pi0[1],
					  P4_1[2]+P4_Pi0[2],
					  E1+P4_Pi0[3]);
	double M = P4.M();
	if(range1<M&&M<range2){
		return true;
	}else{
		return false;
	}

}


bool bkg_veto(int track1,double m1, int track2,double m2,vector<vector<double> >* AllTracks, double range1, double range2){

	int get1=0,get2=0;
	vector<double> P4_1,P4_2;
	for(int i=0;i<AllTracks->size();i++){
		vector<double> Track = AllTracks -> at(i);
		if(Track[3]==(double)track1){
			P4_1 = AllTracks -> at(i);
			get1 = 1;
		}else if(Track[3]==(double)track2){
			P4_2 =  AllTracks -> at(i);
			get2 = 1;
		}

		if(get1==1&&get2==1){
			break;
		}
	}

	double E1 = sqrt(P4_1[0]*P4_1[0]+P4_1[1]*P4_1[1]+P4_1[2]*P4_1[2]+m1*m1);
	double E2 = sqrt(P4_2[0]*P4_2[0]+P4_2[1]*P4_2[1]+P4_2[2]*P4_2[2]+m2*m2);

	TLorentzVector P4(P4_1[0]+P4_2[0],
					  P4_1[1]+P4_2[1],
					  P4_1[2]+P4_2[2],
					  E1+E2);
	double M = P4.M();
	if(range1<M&&M<range2){
		return true;
	}else{
		return false;
	}

}




























