#include"header.h"
#include"functions.cxx"
#include<iostream>
#include<string>
using namespace RooFit;
using namespace std;

void Fit_Mbc_shape(string inputTxtName){

    string var_dERange[2];
	double var_argus[2];

	string inputName;
	string cateName;
	string title;
	string path;

	string line;
    std::ifstream inputTxt(inputTxtName.c_str());
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="fit_dERange"){
			var_dERange[0] = b;
			var_dERange[1] = c;
		}else if(a=="fit_argus"){
			var_argus[0] = string2double(b);
			var_argus[1] = string2double(c);
		}else if(a=="fit_inputName"){
			inputName = b;
		}else if(a=="fit_cateName"){
			cateName = b;
		}else if(a=="fit_title"){
			title = b;
		}else if(a=="fit_path"){
			path = b;
		}
	}

	cout<<path+inputName<<endl;
	TFile *inputFile = new TFile((path+inputName).c_str());
    TTree *inputTree = (TTree*)inputFile->Get("output");
     
	RooRealVar Mbc("Mbc","mbc M(GeV)",2.25,2.3);


	string inputShapeName = path + cateName + "_pdf.root";
	TFile * inputShape = new TFile(inputShapeName.c_str());
	RooKeysPdf* sigPdf = inputShape->Get("sigPDF");	

    RooRealVar argpar("argpar","argus shape parameter",var_argus[0],var_argus[1]);
    RooArgusBG bkgPdf("bkgpdf","Argus PDF",Mbc,RooConst(2.3),argpar);

	int evtNums = inputTree->GetEntries();
    RooRealVar nsig("nsig","#signal Events",0,evtNums);
    RooRealVar nbkg("nbkg","#background Events",0,evtNums);
    
	RooAddPdf model("model","sig+bkg",RooArgList(*sigPdf,bkgPdf),RooArgList(nsig,nbkg));
	
    string deltaE_s = var_dERange[0] + "<dE&&dE<"+var_dERange[1];
	TCut dE_cut  = deltaE_s.c_str();
    TCut Mbc_cut = "2.25<Mbc&&Mbc<2.3";
	TCut cut = dE_cut&&Mbc_cut;

    TTree *Tree = inputTree->CopyTree(cut);
	RooDataSet data = RooDataSet("data","data after cut",Tree,Mbc);

    int nevts = Tree->GetEntries(cut);

	RooFitResult *result = model.fitTo(data,Save(kTRUE),Extended());

    RooPlot* frame = Mbc.frame() ; 
    data.plotOn(frame) ; 
    model.plotOn(frame) ;   
	model.plotOn(frame,Components(*sigPdf)) ;   
    model.plotOn(frame,Components(bkgPdf),LineStyle(kDashed),LineColor(kRed)) ;   

	frame -> SetTitle(("mass dsitribution of "+title).c_str());
    frame->Draw() ;
	string saveName = cateName + "_fit.png";
	
	cout<<"signal\t"<<nsig.getValV()<<"\terror\t"<<nsig.getError()<<endl;
	cout<<"background\t"<<nbkg.getValV()<<"\terror\t"<<nbkg.getError()<<endl;
    
}

void Fit_Mbc_shapeCovg(string inputTxtName){

    string var_dERange[2];
	double var_argus[2];
	double var_gmean[2];
	double var_gsigma[2];

	string inputName;
	string cateName;
	string title;
	string path;
	string line;
    std::ifstream inputTxt(inputTxtName.c_str());
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="fit_dERange"){
			var_dERange[0] = b;
			var_dERange[1] = c;
		}else if(a=="fit_argus"){
			var_argus[0] = string2double(b);
			var_argus[1] = string2double(c);
		}else if(a=="fit_inputName"){
			inputName = b;
		}else if(a=="fit_cateName"){
			cateName = b;
		}else if(a=="fit_title"){
			title = b;
		}else if(a=="fit_gmean"){
			var_gmean[0] = string2double(b);
			var_gmean[1] = string2double(c);
		}else if(a=="fit_gsigma"){
			var_gsigma[0] = string2double(b);
			var_gsigma[1] = string2double(c);
		}else if(a=="fit_path"){
			path = b;
		}
	}
	
	TFile *inputFile = new TFile((path+inputName).c_str());
    TTree *inputTree = (TTree*)inputFile->Get("output");
     
	RooRealVar Mbc("Mbc","mbc M(GeV)",2.25,2.3);

    RooRealVar gmean("mean","",var_gmean[0],var_gmean[1]);
    RooRealVar gsigma("sigma","",var_gsigma[0],var_gsigma[1]);
    RooGaussian covg("covg","the gauss to cov mc shape",Mbc,gmean,gsigma);
	
	string inputShapeName = path + cateName + "_pdf.root";
	TFile * inputShape = new TFile(inputShapeName.c_str());
	RooKeysPdf* mcShape = inputShape->Get("sigPDF");	
			
	RooFFTConvPdf sigPdf("sigPdf","",Mbc,*mcShape,covg);

    RooRealVar argpar("argpar","argus shape parameter",var_argus[0],var_argus[1]);
    RooArgusBG bkgPdf("bkgpdf","Argus PDF",Mbc,RooConst(2.3),argpar);

	int evtNums = inputTree->GetEntries();
    RooRealVar nsig("nsig","#signal Events",0,evtNums);
    RooRealVar nbkg("nbkg","#background Events",0,evtNums);
    
	RooAddPdf model("model","sig+bkg",RooArgList(sigPdf,bkgPdf),RooArgList(nsig,nbkg));
	
    string deltaE_s = var_dERange[0] + "<dE&&dE<"+var_dERange[1];
	TCut dE_cut  = deltaE_s.c_str();
    TCut Mbc_cut = "2.25<Mbc&&Mbc<2.3";
	TCut cut = dE_cut&&Mbc_cut;

    TTree *Tree = inputTree->CopyTree(cut);
	RooDataSet data = RooDataSet("data","data after cut",Tree,Mbc);

    int nevts = Tree->GetEntries(cut);

	RooFitResult *result = model.fitTo(data,Save(kTRUE),Extended());
	
    RooPlot* frame = Mbc.frame(2.25,2.3,30); 

	data.plotOn(frame,MarkerColor(12),MarkerStyle(2)); 
	model.plotOn(frame,Components(sigPdf),LineColor(kBlue),LineWidth(2));   
    model.plotOn(frame,Components(bkgPdf),LineStyle(kDashed),LineColor(kGreen),LineWidth(2));   
    model.plotOn(frame,LineWidth(2),LineColor(kRed)) ;   
	frame -> SetTitle(("mass dsitribution of "+title).c_str());
    frame -> GetXaxis() -> SetNdivisions(6);
    frame -> GetYaxis() -> SetNdivisions(6);
	
	frame->Draw();

	TLatex lt;
    lt.SetNDC();
    lt.SetTextAngle(0);
    lt.SetTextSize(0.1);
    lt.SetTextFont(22);
    lt.DrawLatex(0.3,0.7,title.c_str());


	cout<<"signal\t"<<nsig.getValV()<<"\terror\t"<<nsig.getError()<<endl;
	cout<<"background\t"<<nbkg.getValV()<<"\terror\t"<<nbkg.getError()<<endl;
   
	inputFile -> Close();
}

void Fit_Mbc_CB(string inputTxtName){

    string var_dERange[2];
	double var_alpha[2];
	double var_sigma[2];
	double var_x0[2];
	double var_n[2];
	double var_argus[2];
	string path;
	string inputName;
	string cateName;
	string title;

	string line;
    std::ifstream inputTxt(inputTxtName.c_str());
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
        if(a=="fit_dERange"){
			var_dERange[0] = b;
			var_dERange[1] = c;
		}else if(a=="fit_alpha"){
			var_alpha[0] = string2double(b);
			var_alpha[1] = string2double(c);
	    }else if(a=="fit_sigma"){
			var_sigma[0] = string2double(b);
			var_sigma[1] = string2double(c);
		}else if(a=="fit_x0"){
			var_x0[0] = string2double(b);
			var_x0[1] = string2double(c);
	    }else if(a=="fit_n"){
			var_n[0] = string2double(b);
			var_n[1] = string2double(c);
		}else if(a=="fit_argus"){
			var_argus[0] = string2double(b);
			var_argus[1] = string2double(c);
		}else if(a=="fit_inputName"){
			inputName = b;
		}else if(a=="fit_cateName"){
			cateName = b;
		}else if(a=="fit_title"){
			title = b;
		}else if(a=="fit_path"){
			path = b;
		}
	}
	
	TFile *inputFile = new TFile((path+inputName).c_str());
    TTree *inputTree = (TTree*)inputFile->Get("output");
     
	RooRealVar Mbc("Mbc","mbc M(GeV)",2.25,2.3);

	RooRealVar alpha("alpha","alpha",var_alpha[0],var_alpha[1]);
	RooRealVar sigma("sigma","sigma",var_sigma[0],var_sigma[1]);
	RooRealVar x0("x0","x0",var_x0[0],var_x0[1]);
	RooRealVar n("n","n",var_n[0],var_n[1]);
		
	RooCBShape sigPdf("crystal","signal with the shape of crystal ball",Mbc,x0,sigma,alpha,n);

    RooRealVar argpar("argpar","argus shape parameter",var_argus[0],var_argus[1]);
    RooArgusBG bkgPdf("bkgpdf","Argus PDF",Mbc,RooConst(2.3),argpar);

	int evtNums = inputTree->GetEntries();
    RooRealVar nsig("nsig","#signal Events",0,evtNums);
    RooRealVar nbkg("nbkg","#background Events",0,evtNums);
    
	RooAddPdf model("model","sig+bkg",RooArgList(sigPdf,bkgPdf),RooArgList(nsig,nbkg));
	
    string deltaE_s = var_dERange[0] + "<dE&&dE<"+var_dERange[1];
	TCut dE_cut  = deltaE_s.c_str();
    TCut Mbc_cut = "2.25<Mbc&&Mbc<2.3";
	TCut cut = dE_cut&&Mbc_cut;

    TTree *Tree = inputTree->CopyTree(cut);
	RooDataSet data = RooDataSet("data","data after cut",Tree,Mbc);

    int nevts = Tree->GetEntries(cut);

	RooFitResult *result = model.fitTo(data,Save(kTRUE),Extended());

    RooPlot* frame = Mbc.frame() ; 
    data.plotOn(frame) ; 
    model.plotOn(frame) ;   
	model.plotOn(frame,Components(sigPdf)) ;   
    model.plotOn(frame,Components(bkgPdf),LineStyle(kDashed),LineColor(kRed)) ;   

	frame -> SetTitle(("mass dsitribution of "+title).c_str());
    frame->Draw() ;
	
	cout<<"signal\t"<<nsig.getValV()<<"\terror\t"<<nsig.getError()<<endl;
	cout<<"background\t"<<nbkg.getValV()<<"\terror\t"<<nbkg.getError()<<endl;
    
}



void Fit_combine(string inputTxtName){

	int shapeSwitch = 0;
	int shapeCovgSwitch = 0;

	cout<<inputTxtName<<endl;
	string line;
    std::ifstream inputTxt(inputTxtName.c_str());
    while(getline(inputTxt, line)) {
        if (!line.length() || line[0] == '#')
           continue;
							    
		std::istringstream iss(line);
        string a,b,c;
        iss>>a>>b>>c;
    
		if(a=="fit_shape"){
			shapeSwitch = (int) string2double(b);
		}else if(a=="fit_shapeCovg"){
			shapeCovgSwitch = (int) string2double(b);
		}
	}

	if(shapeSwitch==0){
		Fit_Mbc_CB(inputTxtName);
	}else{
		if(shapeCovgSwitch==0){
			Fit_Mbc_shape(inputTxtName);
		}else{
			Fit_Mbc_shapeCovg(inputTxtName);
		}
	}
	

}














